//
//  BerandaController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 22/11/22.
//

import UIKit
import Kingfisher
import Alamofire

class BerandaController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var verificationView: UIView!
    @IBOutlet weak var collectionView1: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    @IBOutlet weak var modalBackgroundView: UIView!
    @IBOutlet weak var notifLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var arrayIconMenu1: [String] = ["https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiEzIZSl-y3bxdAHBwK2BKsy81oxt_KvkCJAelqquuymLbYxxdryz7OF1N2Sd0ybfNr_B192DH5mZ1ZbM1X9xC4r70Owr3GQexnfRtjPdv2dG2PV_4q6dN7QV3KkfSEqgRf5bN6tIaCxNIHDfIMLQ9dbhEhEolm6-p9zGbzAVgjQOrYf2rD8G9icgwqRw/s1600/9dede448-0927-4895-9d35-040bb018a57f.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEi5tb7ChX4FJTIM-_2S3SeA_kVHoBl4S2SWN6GPWXbdb7dr6FIvGV-Aa-nAsPOsdwKOa6xXWO1dbB3FGhe-zy__Mlxy47bnpK82oeW-lZBPw67ajdnNI5a-T1vS7c6OYVivhnfmbL1Fq5oCyEwAs6z9WnyTMCGOm4QI-9Sohk84nUtt0I10f8-f1v0e2g/s1600/0b4969bd-3f49-40c8-a147-28eb15a892ad.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhX2V22bezIM28hkx_XXsvJomGsuWelxz8j5df5HoU1WA4D9sgMcM-fdJXHQMFKtAT5i6sTqTlgvG3ldtCVLeAcACfuPyhaP1Desk9bseKx9Ts-rnteetLbVPpitWtyFeQ8RWPt050JxjTdMPKr2GqvPfPN6dYLpppI1kOZBNV-nri9ESKTcUWV8XZ7cg/s1600/d2a22b13-4e64-4bea-84a8-ab04660a99fe.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEhx5OtBXUfRYmSqWtEClk0RIUjObMiQaIqsKB0lRftxnJJObmELCFFNo57D45NYXYWeyfIVgQJQTPpWYXTTgOknP5R5Hz9lkpY-BQbpz3WvFJ8Cr7cBwK6SfyfcCis07SHP4WyEPotboVGhAexJbJ_i6oxYvRjEoVAunQF2btbgmnZc6ACwfiBfD2XuFg/s1600/43821da2-64a8-4228-b78a-d2c616f1eccd.JPG"]
    var arrayIconMenu2: [String] = ["https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEiNFxKmeBkrpEn4Q-p7v2jqV86kMZwFD8S3Cp-M-bx_cwLrv4YzeRl33h4_mXgI2qcci46RCKsaDv8vsGtz_UaNp6FqJrxWA3o-iAz8ExbEIcxQulbsTXuyuxRfnLMKPdiJlILPHPi1QiHruKnlC9cuZg_cAnqIlnO8AFoGvOKzxa-ufKxHq8pm9kBccA/s1600/cf909fd7-53b5-4ef9-b15b-f24912753a57.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEg3kgvDue1RFSdWPa9MRuxaIHiGMDzdEEAUzNok562V7G1K69NujKsUBlzZTmAptg69EYbOvbBuSL2SyMRskKKUqLNw-cy5oz-VHEsaM34Qe2gzQsXoRhXoPgIEimt7yRO590WKOgX7NAtSF_ms2cg9n6xn27MnEu1N7i0N9ReO_8kWmjHzpoU_fdm2ZA/s1600/bc5b1951-ea85-4a38-908e-d57959a2282d.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgpi3vPAnk85xB0-o3aWMN2NXSC2__fX1Kua8t6wWkkX1WPPBIioJRsIsDO9lJ0uRaoMiqHtcoCYLqa0gyYysD9bXa7xzdq_AYJLOzZaflNCyrvQPng64El5BQxBoO7dYBYffpEsfewaJyXq99x00CezTn5Kmq0x-Bl-epWrqG3JjrTt_lbx3ZGU9A1ig/s1600/5a33c350-b054-49d6-bc6c-3cbd5e5a9190.JPG", "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEi_ngyu-48C0ZNY_ZHE3qPxMugpFABYdvMUpSkRDS6Pl3L9QlGKTKe4MW38z9FJib2K7oLF-NbvBiy_ZDLZ5HPteWisz5gzpZJ_nQOuyXwa8mGqDXMOC_wFpX24DU1enYrwKqimFH7DkGDxlMYzd_ldlsn4twSyTPP1TCYsv5OdQkH89bLEeZmVSCMFwQ/s1600/4f1cd01c-3af4-4f69-9170-de44c0028dbb.JPG"]
    var arrayLabelMenu1 = ["Angsuran", "Pulsa", "Paket Data", "PLN"]
    var arrayLabelMenu2 = ["Telkom", "Internet & Kabel", "Asuransi", "Lainnya"]
    var arrayPromosiImage: [String] = ["https://www.astrapay.com/static-assets/images/upload/promo/main/ppob17nov_903x510.png", "https://www.astrapay.com/static-assets/images/upload/promo/main/dayaauto_903x510.png", "https://www.astrapay.com/static-assets/images/upload/promo/main/transjatim_sept_903x510.png", "https://www.astrapay.com/static-assets/images/upload/promo/main/Trans%20jogja_sept2022_903x510_AP.jpg"]
    var arrayProdukImage: [String] = [ "https://maucash.id/wp-content/uploads/2022/10/Maumodal-app-banner-AP-03.jpg", "https://maucash.id/wp-content/uploads/2022/10/Astrapay-November-22-02-scaled.jpg", "https://astrapay.com/static-assets/images/upload/blog/main/2022/10/fraudawareness_onlineshop_blog903x903.png", "https://astrapay.com/static-assets/images/upload/blog/main/2022/07/externalfraud_juli_gantiPIN_1080x1080.png"]
    var arrayProdukLabel = ["Maucash", "Maupaylater", "Maumodal", "Maupaypoints"]
    var dataWallet: [String : Any]?
    var dataUser: [String : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView1.delegate = self
        tableView1.dataSource = self
        
        verificationView.layer.cornerRadius = 10
        verificationView.layer.borderWidth = 1
        verificationView.layer.borderColor = UIColor.orange.cgColor
        
        collectionView1.delegate = self
        collectionView1.dataSource = self
        collectionView1.isScrollEnabled = false
        
        collectionView2.delegate = self
        collectionView2.dataSource = self
       
        collectionView3.delegate = self
        collectionView3.dataSource = self
        
        modalBackgroundView.layer.cornerRadius = 20.0
       
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController!.navigationBar.backgroundColor = .clear
        self.navigationController?.navigationBar.backItem?.title = ""
        
        nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        
        let boldAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold)
        ]
        let regularAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .light)
        ]
        let boldText = NSAttributedString(string: "AstraPocket", attributes: boldAttribute)
        let regularText1 = NSAttributedString(string: " Yuk, menabung bersama ", attributes: regularAttribute)
        let regularText2 = NSAttributedString(string: "!", attributes: regularAttribute)
        let newString = NSMutableAttributedString()
        newString.append(regularText1)
        newString.append(boldText)
        newString.append(regularText2)
        notifLabel.attributedText = newString
        
        getWallet()
        getCurrentUser()
    }
    
    // T A B L E  V I E W
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
            cell.selectionStyle = .none
            
            let settingButton = cell.viewWithTag(2) as! UIButton
            settingButton.addTarget(self, action: #selector(settingButtonTapped), for: .touchUpInside)
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
            cell.selectionStyle = .none
            
            let labelRow2 = cell.viewWithTag(1) as! UILabel
            let nama = dataUser?["nama"] as? String ?? ""
            labelRow2.text = "Halo, \(nama)"
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3", for: indexPath)
            cell.selectionStyle = .none
            
            let viewRow3 = cell.viewWithTag(1)!
            viewRow3.layer.cornerRadius = 10
            
            let classicView = cell.viewWithTag(2)!
            classicView.layer.cornerRadius = 18
            
            let bottomView = cell.viewWithTag(3)!
            bottomView.layer.cornerRadius = 10
            bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
            let saldoLabel = cell.viewWithTag(4) as! UILabel
            let saldo = dataWallet?["saldo"] as? Int ?? 0
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "id_ID")
            formatter.groupingSeparator = "."
            formatter.numberStyle = .decimal
            let formattedSaldoAmount = formatter.string(from: saldo as NSNumber) ?? ""
            saldoLabel.text = "Rp\(formattedSaldoAmount)"
           
            return cell
        }
    }
    
    @objc func settingButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfilController") as! ProfilController
        viewController.dataUser = self.dataUser!
        self.navigationController?.pushViewController(viewController, animated: true)
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section, indexPath.row)
        
    }
    
    // C O L L E C T I O N  V I E W
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.collectionView1 {
            return 2
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView1 {
            if section == 0 {
                return arrayIconMenu1.count
            } else {
                return arrayIconMenu2.count
            }
        } else if collectionView == self.collectionView2 {
            return arrayPromosiImage.count
        } else {
            return arrayPromosiImage.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.section, indexPath.row)
        if indexPath.row == 3 && indexPath.section == 1 {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuLainnyaController") as! MenuLainnyaController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView1 {
            if indexPath.section == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
                
                let image1 = cell.viewWithTag(1) as! UIImageView
                let itemImage = arrayIconMenu1[indexPath.row]
                image1.kf.setImage(with: URL(string: itemImage))
                
                let label1 = cell.viewWithTag(2) as! UILabel
                label1.text = arrayLabelMenu1[indexPath.row]
          
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)
                
                let image2 = cell.viewWithTag(1) as! UIImageView
                let itemImage = arrayIconMenu2[indexPath.row]
                image2.kf.setImage(with: URL(string: itemImage))
                
                let label2 = cell.viewWithTag(2) as! UILabel
                label2.text = arrayLabelMenu2[indexPath.row]
         
                return cell
            }
        } else if collectionView == self.collectionView2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath)
            
            let image1 = cell.viewWithTag(1) as! UIImageView
            let itemImage = arrayPromosiImage[indexPath.row]
            image1.kf.setImage(with: URL(string: itemImage))
            view.layer.cornerRadius = 20
            cell.layer.cornerRadius = 20
            cell.layer.masksToBounds = true

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell4", for: indexPath)
            
            let image1 = cell.viewWithTag(1) as! UIImageView
            let itemImage = arrayProdukImage[indexPath.row]
            image1.kf.setImage(with: URL(string: itemImage))
            image1.layer.cornerRadius = 20
            image1.layer.masksToBounds = true
            
            let label1 = cell.viewWithTag(2) as! UILabel
            label1.text = arrayProdukLabel[indexPath.row]
       
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView1 {
            return CGSize.init(width: 77, height:100)
        } else if collectionView == self.collectionView2 {
            return CGSize.init(width: 120, height:100)
        } else {
            return CGSize.init(width: 120, height:100)
        }
    }
    
    @objc func nextButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPocketController") as! DashboardPocketController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func getWallet() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-wallet-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            let code = itemObject?["status"] as? Int
        
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            } else {
                                self.dataWallet = data!
                                self.tableView1.reloadData()
                            }
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    func getCurrentUser() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-current-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            let code = itemObject?["status"] as? Int
                                
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            } else {
                                self.dataUser = data!
                                self.tableView1.reloadData()
                            }
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
}

