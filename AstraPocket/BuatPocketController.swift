//
//  BuatPocketController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class BuatPocketController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet weak var kategoriTextField: UITextField!
    @IBOutlet weak var namaPocketTextField: UITextField!
    @IBOutlet weak var buatPocketView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nominalTargetTextField: UITextField!
    @IBOutlet weak var buatPocketButton: UIButton!
    @IBOutlet weak var gantiButton: UIButton!
    @IBOutlet weak var editGambarView: UIView!
    @IBOutlet weak var editGambarButton: UIButton!
    @IBOutlet weak var gambarPocketImage: UIImageView!
    @IBOutlet weak var sisaNominalTargetLabel: UILabel!
    
    var dataKategori : [String : Any] = [:]
    var dataSaldoPocket : [String : Any] = [:]
    var namaPocket = ""
    var nominalTarget = ""
    var amt: Int = 0
    let formatter = NumberFormatter()
    var linkGambar = ""
    var imagePicker = UIImagePickerController()
    var image = UIImage()
    var imageName = ""
    var gambarPocket = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buatPocketView.layer.shadowColor = UIColor.gray.cgColor
        buatPocketView.layer.shadowOpacity = 0.3
        buatPocketView.layer.shadowOffset = .zero
        buatPocketView.layer.shadowRadius = 5
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        buatPocketButton.addTarget(self, action: #selector(buatPocketButtonTapped), for: .touchUpInside)
        gantiButton.addTarget(self, action: #selector(gantiButtonTapped), for: .touchUpInside)
        editGambarButton.addTarget(self, action: #selector(editGambarButtonTapped), for: .touchUpInside)

        self.navigationController?.navigationBar.isHidden = true
        
        let bottomLineKategoriPocket = CALayer()
        bottomLineKategoriPocket.frame = CGRect(x: 0.0, y: kategoriTextField.frame.height - 1, width: kategoriTextField.frame.width, height: 1.0)
        bottomLineKategoriPocket.backgroundColor = UIColor.systemGray5.cgColor
        kategoriTextField.borderStyle = UITextField.BorderStyle.none
        kategoriTextField.layer.addSublayer(bottomLineKategoriPocket)
        
        let bottomLineNamaPocket = CALayer()
        bottomLineNamaPocket.frame = CGRect(x: 0.0, y: nominalTargetTextField.frame.height - 1, width: nominalTargetTextField.frame.width, height: 1.0)
        bottomLineNamaPocket.backgroundColor = UIColor.systemGray5.cgColor
        nominalTargetTextField.borderStyle = UITextField.BorderStyle.none
        nominalTargetTextField.layer.addSublayer(bottomLineNamaPocket)
        nominalTargetTextField.delegate = self
        nominalTargetTextField.placeholder = updateAmount()
        nominalTargetTextField.clearButtonMode = .always
        nominalTargetTextField.clearButtonMode = .whileEditing
        
        let bottomLineNominalTarget = CALayer()
        bottomLineNominalTarget.frame = CGRect(x: 0.0, y: namaPocketTextField.frame.height - 1, width: namaPocketTextField.frame.width, height: 1.0)
        bottomLineNominalTarget.backgroundColor = UIColor.systemGray5.cgColor
        namaPocketTextField.borderStyle = UITextField.BorderStyle.none
        namaPocketTextField.layer.addSublayer(bottomLineNominalTarget)
        namaPocketTextField.clearButtonMode = .always
        namaPocketTextField.clearButtonMode = .whileEditing
        
        editGambarView.layer.cornerRadius = 15
        editGambarView.layer.borderWidth = 2.5
        editGambarView.layer.borderColor = UIColor.white.cgColor
        
        gambarPocket = dataKategori["gambarKategori"] as? String ?? ""
        
        linkGambar = "http://localhost:8080/files/\(gambarPocket)\(":.+")"
        gambarPocketImage.kf.setImage(with: URL(string: linkGambar ))
        gambarPocketImage.layer.cornerRadius = gambarPocketImage.layer.frame.width / 2
        
        let kategoriPocket = dataKategori["namaKategori"] as? String ?? ""
        kategoriTextField.text = kategoriPocket
        kategoriTextField.isEnabled = false
        
        namaPocketTextField.text = ""
        nominalTargetTextField.text = "Rp"
        buatPocketButton.isEnabled = false
        
        getPocket()
        
    }
    
    @IBAction func nominalValidation(_ sender: Any) {
        if let nominal = Int(nominalTargetTextField.text!.onlyDigits) {
            if let errorMessage = invalidNominal(nominal){
                sisaNominalTargetLabel.text = errorMessage
                sisaNominalTargetLabel.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1.0)
                sisaNominalTargetLabel.isHidden = false
            } else {
                let sisaNominalTarget = dataSaldoPocket["sisaNominalTarget"] as? Int ?? 0
                let formattedSisaNominalTargetAmount = self.formatter.string(from: sisaNominalTarget as NSNumber) ?? ""
                self.sisaNominalTargetLabel.text = "Maksimum nominal target adalah \(formattedSisaNominalTargetAmount)"
                sisaNominalTargetLabel.textColor = .black
            }
        }
        checkValidasiForm()
    }
    
//    func invalidNominalTargetTextField(_ value: String) -> String? {
//        if (value == ""){
//            return "Nominal target tidak boleh kosong"
//        }
//        return nil
//    }
    

    @IBAction func namaPocketValidation(_ sender: Any) {
        let namaPocket = namaPocketTextField.text!
        invalidNamaPocket(namaPocket)
        checkValidasiForm()
    }
    
    func invalidNamaPocket(_ value: String) -> String? {
        if (value == ""){
            return "Nama tidak boleh kosong"
        }
        return nil
    }

    func checkValidasiForm() {
        if sisaNominalTargetLabel.textColor != UIColor(red: 255, green: 0, blue: 0, alpha: 1.0) && nominalTargetTextField.text != "Rp" && namaPocketTextField.text != "" && nominalTargetTextField.text != "" {
            buatPocketButton.isEnabled = true
        } else {
            buatPocketButton.isEnabled = false
        }
    }
    
    func invalidNominal(_ value: Int) -> String? {
        let sisaNominalTarget = dataSaldoPocket["sisaNominalTarget"] as? Int ?? 0
        
        if (value > sisaNominalTarget){
            return "Nominal target tidak boleh melebihi batas maksimum nominal target"
        }
        if (value == 0){
            return "Minimum nominal target adalah Rp1"
        }
        return nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let digit = Int(string) {
            amt = amt * 10 + digit
            nominalTargetTextField.text = updateAmount()
        }
        if string == "" {
            amt = amt/10
            nominalTargetTextField.text = amt == 0 ? "" : updateAmount()
        }
        nominalValidation(amt)
        return false
    }
    
    func updateAmount() -> String? {
        formatter.numberStyle = NumberFormatter.Style.currency
        let amount = amt
        
        return formatter.string(from: NSNumber(value: amount))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buatPocketButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let idKategori = dataKategori["id"] as? Int ?? 0
    
        namaPocket = namaPocketTextField.text!
        nominalTarget = nominalTargetTextField.text!
        nominalTarget = nominalTarget.onlyDigits
        
        self.image = gambarPocketImage.image!
        let newFileName = "pocket_photo_\(Int.random(in: 0 ... 100000000000)).jpeg"
        
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(self.image.jpegData(compressionQuality: 0.1)!, withName: "file" , fileName: newFileName, mimeType: "image/jpeg")
                print("gambarnya adalah \(newFileName)")
            },
            to: "http://localhost:8080/upload-file", method: .post , headers: headers)
        .response { resp in
            print(resp)
        }
        
        gambarPocket = dataKategori["gambarKategori"] as? String ?? ""
        linkGambar = "http://localhost:8080/files/\(gambarPocket)\(":.+")"
        gambarPocketImage.kf.setImage(with: URL(string: linkGambar))
        
        let param = ["pocketDetail":[["namaPocket": namaPocket, "nominalTarget": nominalTarget, "gambarPocket": newFileName ]]]
        AF.request("http://localhost:8080/tambah-pocket-detail/\(idKategori)", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]
                            
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPocketController") as! DashboardPocketController
                            self.navigationController?.pushViewController(viewController2, animated: true)
                            
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            viewController.providesPresentationContextTransitionStyle = true;
                            viewController.definesPresentationContext = true;
                            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                            self.present(viewController, animated: true, completion: {
                                viewController.view.superview?.isUserInteractionEnabled = true
                                viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                })
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func editGambarButtonTapped(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        gambarPocketImage.image = image
        self.image = image
    }
    
    @objc func gantiButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    func getPocket() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-saldo-pocket", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                    
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let sisaNominalTarget = data?["sisaNominalTarget"] as? Int ?? 0
                            let formattedSisaNominalTargetAmount = self.formatter.string(from: sisaNominalTarget as NSNumber) ?? ""
                            self.sisaNominalTargetLabel.text = "Maksimum nominal target adalah \(formattedSisaNominalTargetAmount)"

                            self.dataSaldoPocket = data!
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
}
