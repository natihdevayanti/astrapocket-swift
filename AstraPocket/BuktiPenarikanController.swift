//
//  BuktiPenarikanController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit

class BuktiPenarikanController: UIViewController {

    @IBOutlet weak var kembaliButton: UIButton!
    @IBOutlet weak var cekDetailTransaksiButton: UIButton!
    @IBOutlet weak var jumlahPenarikanLabel: UILabel!
    
    var jumlahPenarikan = ""
    var dataDetailPocket : [String : Any] = [:]
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        kembaliButton.addTarget(self, action: #selector(kembaliButtonTapped), for: .touchUpInside)
        cekDetailTransaksiButton.addTarget(self, action: #selector(cekDetailTransaksiButtonTapped), for: .touchUpInside)
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        jumlahPenarikan = jumlahPenarikan.onlyDigits
        let jumlahPenarikanIntFormat = Int(jumlahPenarikan)
        let formattedJumlahPenarikanAmount = self.formatter.string(from: jumlahPenarikanIntFormat! as NSNumber) ?? ""
        jumlahPenarikanLabel.text = "Penarikan pocket sebesar Rp\(formattedJumlahPenarikanAmount) ke saldo AstraPay berhasil"
        
        kembaliButton.layer.borderWidth = 1
        kembaliButton.layer.borderColor = UIColor.blue.cgColor
        kembaliButton.layer.cornerRadius = 5.0
    }
    
     @objc func kembaliButtonTapped() {
         let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController
         self.navigationController?.pushViewController(viewController, animated: true)
     }
    
    @objc func cekDetailTransaksiButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailTransaksiController") as! DetailTransaksiController
        viewController.dataDetailPocket = self.dataDetailPocket
        viewController.jumlahPenarikan = self.jumlahPenarikan
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
