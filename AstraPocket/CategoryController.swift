//
//  CategoryController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class CategoryController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    @IBOutlet weak var kategoriTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
   
    var dataTable: [[String : Any]]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backItem?.title = ""
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        kategoriTableView.delegate = self
        kategoriTableView.dataSource = self
        
        getCategory()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
       
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return dataTable?.count ?? 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
           var item = self.dataTable?[indexPath.row]
           var labelTable = cell.viewWithTag(2) as! UILabel
           var imageTable = cell.viewWithTag(1) as! UIImageView
           var nextButton = cell.viewWithTag(3) as! UIButton
           
           labelTable.text = item?["namaKategori"] as? String
           
           var itemGambarKategori = item?["gambarKategori"] as? String ?? ""
           var link = "http://localhost:8080/files/\(itemGambarKategori)\(":.+")"
           imageTable.kf.setImage(with: URL(string: link ?? "" ))

           nextButton.imageView?.contentMode = .scaleAspectFit
           nextButton.imageEdgeInsets = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
           
           return cell
    }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           print(indexPath.section, indexPath.row)
           let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BuatPocketController") as! BuatPocketController
           viewController.dataKategori = self.dataTable![indexPath.row]
           self.navigationController?.pushViewController(viewController, animated: true)
    }
       
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 70
    }

    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getCategory() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-kategori", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]
                            
                            self.dataTable = data!
                            self.kategoriTableView.reloadData()

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }

}
