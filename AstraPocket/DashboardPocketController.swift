//
//  DashboardPocketController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import SwiftUI
import Kingfisher
import Alamofire

class DashboardPocketController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var pocketCollectionView: UICollectionView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var buatPocketButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var totalSaldoLabel: UILabel!
    @IBOutlet weak var semuaPocketButton: UIButton!
    @IBOutlet weak var sudahTerpenuhiButton: UIButton!
    @IBOutlet weak var belumTerpenuhiButton: UIButton!
    @IBOutlet weak var judulNotifLabel: UILabel!
    @IBOutlet weak var iconNotifImage: UIImageView!
    @IBOutlet weak var isiNotifLabel: UILabel!
    
    var dataCollectionView: [[String : Any]]?
    var dataCollectionViewSemua = true
    var dataCollectionViewBelumTerpenuhi = false
    var dataCollectionViewSudahTerpenuhi = false
    var dataCollectionViewBelumTerpenuhiArray: [[String : Any]]?
    var dataCollectionViewSudahTerpenuhiArray: [[String : Any]]?
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        buatPocketButton.layer.cornerRadius = 10
        buatPocketButton.clipsToBounds = true
 
        pocketCollectionView.delegate = self
        pocketCollectionView.dataSource = self
        
        semuaPocketButton.addTarget(self, action: #selector(semuaPocketButtonTapped), for: .touchUpInside)
        belumTerpenuhiButton.addTarget(self, action: #selector(belumTerpenuhiButtonTapped), for: .touchUpInside)
        sudahTerpenuhiButton.addTarget(self, action: #selector(sudahTerpenuhiButtonTapped), for: .touchUpInside)
        
        semuaPocketButton.setTitle("Semua", for: .normal)
        semuaPocketButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        semuaPocketButton.setTitleColor(.white, for: .normal)
        semuaPocketButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1)
        semuaPocketButton.layer.cornerRadius = 10
        semuaPocketButton.clipsToBounds = true
        
        belumTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        belumTerpenuhiButton.layer.cornerRadius = 10
        belumTerpenuhiButton.clipsToBounds = true
        belumTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        belumTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sudahTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sudahTerpenuhiButton.layer.cornerRadius = 10
        sudahTerpenuhiButton.clipsToBounds = true
        sudahTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sudahTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        buatPocketButton.addTarget(self, action: #selector(buatPocketButtonTapped), for: .touchUpInside)
        
        self.navigationController?.navigationBar.isHidden = true
  
        getPocket()
        getSaldoPocket()
       
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        judulNotifLabel.isHidden = true
        iconNotifImage.isHidden = true
        isiNotifLabel.isHidden = true
        
        judulNotifLabel.text = "Belum ada pocket yang kamu buat nih"
        isiNotifLabel.text = "Yuk, mulai menabung sekarang juga!"
        iconNotifImage.image = UIImage.init(named: "no-transaction-icon")
    }
    
    // C O L L E C T I O N  V I E W
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dataCollectionViewSemua == true {
            return dataCollectionView?.count ?? 0
        } else if dataCollectionViewBelumTerpenuhi == true {
            return dataCollectionViewBelumTerpenuhiArray?.count ?? 0
        } else {
            return dataCollectionViewSudahTerpenuhiArray?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)

        var item: [String : Any]?
        
        if dataCollectionViewSemua == true {
            item = self.dataCollectionView?[indexPath.row]
        } else if dataCollectionViewBelumTerpenuhi == true {
            item = self.dataCollectionViewBelumTerpenuhiArray?[indexPath.row]
        } else if dataCollectionViewSudahTerpenuhi == true {
            item = self.dataCollectionViewSudahTerpenuhiArray?[indexPath.row]
        }
        
        let imageCV = cell.viewWithTag(1) as! UIImageView
        let itemGambarKategori = item?["gambarPocket"] as? String ?? ""
        let link = "http://localhost:8080/files/\(itemGambarKategori)\(":.+")"
        imageCV.kf.setImage(with: URL(string: link ))
        imageCV.layer.cornerRadius = imageCV.layer.frame.width / 2

        let cardView = cell.viewWithTag(2)!
        cardView.layer.cornerRadius = 10.0
        cell.layer.borderWidth = 0.0
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false
        
        let titikCard = cell.viewWithTag(3)!
        titikCard.layer.cornerRadius = 3.0
        
        let namaPocket = cell.viewWithTag(4) as! UILabel
        namaPocket.text = item?["namaPocket"] as? String
       
        let kategoriPocket = cell.viewWithTag(5) as! UILabel
        kategoriPocket.adjustsFontSizeToFitWidth = false
        kategoriPocket.lineBreakMode = .byTruncatingTail
        kategoriPocket.text = item?["kategoriPocket"] as? String
    
        let nominalPocket = cell.viewWithTag(6) as! UILabel
        let itemNominalPocket = item?["nominalPocket"] as? Int ?? 0
        
        let formattedNominalPocketAmount = formatter.string(from: itemNominalPocket as NSNumber) ?? ""
        nominalPocket.text = "Rp\(formattedNominalPocketAmount)"
        
        let itemNominalTarget = item?["nominalTarget"] as? Int ?? 0
        let formattedNominalTargetAmount = formatter.string(from: itemNominalTarget as NSNumber) ?? ""
        
        let progressPocket = cell.viewWithTag(7) as! UILabel
        let itemProgressPercentage = item?["progressPocket"] as? Int ?? 0
        progressPocket.text = "\(itemProgressPercentage)% dari Rp\(formattedNominalTargetAmount)"
      
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfItemsPerRow: CGFloat = 2.0
        let minimumCellSpacing: CGFloat = 18.0
        let totalSpacing = (numberOfItemsPerRow + 1)*minimumCellSpacing //left + right + in-between padding
        print("Total Spacing is \(totalSpacing)")
        let width = (collectionView.frame.width - totalSpacing)/numberOfItemsPerRow
        print("The Frame Width is \(collectionView.frame.width)")
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailPocketController") as! DetailPocketController
        viewController.detailPocket = self.dataCollectionView![indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
   
    @objc func buatPocketButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func semuaPocketButtonTapped() {
        dataCollectionViewSemua = true
        dataCollectionViewBelumTerpenuhi = false
        dataCollectionViewSudahTerpenuhi = false
      
        semuaPocketButton.setTitle("Semua", for: .normal)
        semuaPocketButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        semuaPocketButton.setTitleColor(.white, for: .normal)
        semuaPocketButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        semuaPocketButton.layer.cornerRadius = 10
        semuaPocketButton.clipsToBounds = true
        
        belumTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        belumTerpenuhiButton.layer.cornerRadius = 10
        belumTerpenuhiButton.clipsToBounds = true
        belumTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        belumTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sudahTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sudahTerpenuhiButton.layer.cornerRadius = 10
        sudahTerpenuhiButton.clipsToBounds = true
        sudahTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sudahTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        self.pocketCollectionView.reloadData()
    }
    
    @objc func belumTerpenuhiButtonTapped() {
        dataCollectionViewSemua = false
        dataCollectionViewBelumTerpenuhi = true
        dataCollectionViewSudahTerpenuhi = false
        
        belumTerpenuhiButton.setTitle("Belum Terpenuhi", for: .normal)
        belumTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        belumTerpenuhiButton.setTitleColor(.white, for: .normal)
        belumTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        belumTerpenuhiButton.layer.cornerRadius = 10
        belumTerpenuhiButton.clipsToBounds = true
       
        semuaPocketButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        semuaPocketButton.layer.cornerRadius = 10
        semuaPocketButton.clipsToBounds = true
        semuaPocketButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        semuaPocketButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sudahTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sudahTerpenuhiButton.layer.cornerRadius = 10
        sudahTerpenuhiButton.clipsToBounds = true
        sudahTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sudahTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
//        dataCollectionViewBelumTerpenuhiArray?.removeAll()
        dataCollectionViewBelumTerpenuhiArray = []

        for (index, element) in dataCollectionView!.enumerated() {
            let progressPocket = dataCollectionView?[index]["progressPocket"] as? Int ?? 0
            if progressPocket != 100 {
                dataCollectionViewBelumTerpenuhiArray?.append(dataCollectionView![index])
            }
            self.pocketCollectionView.reloadData()
        }
    }
    
    @objc func sudahTerpenuhiButtonTapped() {
        dataCollectionViewSemua = false
        dataCollectionViewBelumTerpenuhi = false
        dataCollectionViewSudahTerpenuhi = true

        sudahTerpenuhiButton.setTitle("Sudah Terpenuhi", for: .normal)
        sudahTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sudahTerpenuhiButton.setTitleColor(.white, for: .normal)
        sudahTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        sudahTerpenuhiButton.layer.cornerRadius = 10
        sudahTerpenuhiButton.clipsToBounds = true
       
        belumTerpenuhiButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        belumTerpenuhiButton.layer.cornerRadius = 10
        belumTerpenuhiButton.clipsToBounds = true
        belumTerpenuhiButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        belumTerpenuhiButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        semuaPocketButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        semuaPocketButton.layer.cornerRadius = 10
        semuaPocketButton.clipsToBounds = true
        semuaPocketButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        semuaPocketButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
//        dataCollectionViewSudahTerpenuhiArray?.removeAll()
        dataCollectionViewSudahTerpenuhiArray = []

        for (index, element) in dataCollectionView!.enumerated() {
            let progressPocket = dataCollectionView?[index]["progressPocket"] as? Int ?? 0
            if progressPocket == 100 {
                dataCollectionViewSudahTerpenuhiArray?.append(dataCollectionView![index])
            }
            self.pocketCollectionView.reloadData()
        }
    }
    
    func getPocket() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-pocket-detail-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            self.dataCollectionView = data!
                            self.pocketCollectionView.reloadData()
                            
                            if data?.count == 0 {
                                self.judulNotifLabel.isHidden = false
                                self.isiNotifLabel.isHidden = false
                                self.iconNotifImage.isHidden = false
                            } else {
                               
                                self.judulNotifLabel.isHidden = true
                                self.isiNotifLabel.isHidden = true
                                self.iconNotifImage.isHidden = true
                            }
                            print("datanya nih \(data)")
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    func getSaldoPocket() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-saldo-pocket", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                           
                            
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let saldoPocket = data?["saldo"] as? Int ?? 0
                            let formattedTotalSaldoAmount = self.formatter.string(from: saldoPocket as NSNumber) ?? ""
                            self.totalSaldoLabel.text = "Rp\(formattedTotalSaldoAmount)"
                            
                            self.pocketCollectionView.reloadData()
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
}
