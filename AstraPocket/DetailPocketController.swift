//
//  TopupSaldoController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire
import Kingfisher

protocol GoToPenarikanConfirmationPageDelegate {
    func pushToPenarikanConfirmationPage(dataNominal: String)
}

protocol GoToTopupConfirmationPageDelegate {
    func pushToTopupConfirmationPage(dataNominal: String)
}

protocol GoToEditPocketPageDelegate {
    func pushToEditPocketPage()
}

protocol GoToDeletePocketPageDelegate {
    func pushToDeletePocketPage()
}

protocol GoToDashboardPocketPageDelegate {
    func pushToDashboardPocketPage()
}

protocol CloseModalDelegate {
    func closeModalAndHideView()
}

class DetailPocketController: UIViewController, UITableViewDelegate, UITableViewDataSource, GoToPenarikanConfirmationPageDelegate, GoToTopupConfirmationPageDelegate, GoToEditPocketPageDelegate, GoToDeletePocketPageDelegate, GoToDashboardPocketPageDelegate, CloseModalDelegate {

    @IBOutlet weak var progressPocketView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var historiPocketTableView: UITableView!
    @IBOutlet weak var topupButton: UIButton!
    @IBOutlet weak var tarikButton: UIButton!
    @IBOutlet weak var settingMenuButton: UIButton!
    @IBOutlet weak var gambarPocketImage: UIImageView!
    @IBOutlet weak var nominalTargetLabel: UILabel!
    @IBOutlet weak var kategoriPocketLabel: UILabel!
    @IBOutlet weak var namaPocketLabel: UILabel!
    @IBOutlet weak var layarView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var detailPocket: [String : Any] = [:]
    var historiPocket: [[String : Any]]? = []
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        tarikButton.addTarget(self, action: #selector(tarikButtonTapped), for: .touchUpInside)
        topupButton.addTarget(self, action: #selector(topupButtonTapped), for: .touchUpInside)
        settingMenuButton.addTarget(self, action: #selector(settingMenuButtonTapped), for: .touchUpInside)
 
        historiPocketTableView.delegate = self
        historiPocketTableView.dataSource = self
        
        backgroundImage.layer.cornerRadius = 20
        
        progressPocketView.layer.cornerRadius = 10.0
        progressPocketView.layer.borderWidth = 0.0
        progressPocketView.layer.shadowColor = UIColor.gray.cgColor
        progressPocketView.layer.shadowOffset = CGSize(width: 0, height: 0)
        progressPocketView.layer.shadowRadius = 5.0
        progressPocketView.layer.shadowOpacity = 0.5
        progressPocketView.layer.masksToBounds = false
        var progressPocket = detailPocket["progressPocket"] as? Float ?? 0.0
        progressView.progress = progressPocket/100
        
        tarikButton.layer.borderWidth = 1
        tarikButton.layer.borderColor = UIColor.blue.cgColor
        tarikButton.layer.cornerRadius = 10
        tarikButton.clipsToBounds = true
        
        topupButton.layer.cornerRadius = 10
        topupButton.clipsToBounds = true
      
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        let gambarPocket = detailPocket["gambarPocket"] as? String ?? ""
        let link = "http://localhost:8080/files/\(gambarPocket)\(":.+")"
        gambarPocketImage.kf.setImage(with: URL(string: link ))
        gambarPocketImage.layer.cornerRadius = gambarPocketImage.layer.frame.width / 2
        
       
        let boldAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .semibold)
        ]
        let regularAttribute = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .light)
        ]
        
        let nominalPocket = detailPocket["nominalPocket"] as? Int ?? 0
        let formattedNominalPocketAmount = formatter.string(from: nominalPocket as NSNumber) ?? ""
        let boldText = NSAttributedString(string: "Rp\(formattedNominalPocketAmount)", attributes: boldAttribute)
        
        let nominalTarget = detailPocket["nominalTarget"] as? Int ?? 0
        let formattedNominalTargetAmount = formatter.string(from: nominalTarget as NSNumber) ?? ""
        let regularText = NSAttributedString(string: " dari Rp\(formattedNominalTargetAmount) terkumpul ", attributes: regularAttribute)
        let newString = NSMutableAttributedString()
        newString.append(boldText)
        newString.append(regularText)
        nominalTargetLabel.attributedText = newString
        
        let namaPocket = detailPocket["namaPocket"] as? String
        namaPocketLabel.text = namaPocket
        
        let kategoriPocket = detailPocket["kategoriPocket"] as? String
        kategoriPocketLabel.text = kategoriPocket
    }
    
    @objc func refresh() {
       self.historiPocketTableView.reloadData()
   }

    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func topupButtonTapped() {
        let progressPocket = detailPocket["progressPocket"] as? Int ?? 0
        
        if progressPocket == 100 {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GagalNotificationController") as! GagalNotificationController
        
            viewController.judulNotif = "Gagal Topup Pocket"
            viewController.isiNotif = "Target tabungan kamu sudah penuh nih!"
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          
            self.present(viewController, animated: true, completion: {
                viewController.view.superview?.isUserInteractionEnabled = true
                viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                })
        } else {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "TopupSaldoController") as! TopupSaldoController
            viewController.dataDetailPocket = self.detailPocket
            layarView.isHidden = false
            
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

            viewController.delegate = self
            self.present(viewController, animated: true, completion: {
            viewController.view.superview?.isUserInteractionEnabled = true
            viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                self.layarView.isHidden = false
            })
        }
    }
    
    @objc func tarikButtonTapped() {
        let nominalPocket = detailPocket["nominalPocket"] as? Int ?? 0
        
        if nominalPocket == 0 {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GagalNotificationController") as! GagalNotificationController
        
            viewController.judulNotif = "Gagal Tarik Pocket"
            viewController.isiNotif = "Yuk, isi saldo pocket kamu dulu!"
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          
            self.present(viewController, animated: true, completion: {
                viewController.view.superview?.isUserInteractionEnabled = true
                viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                })
        } else {
            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "PenarikanSaldoController") as! PenarikanSaldoController
            viewController2.dataDetailPocket = self.detailPocket
            layarView.isHidden = false

            viewController2.providesPresentationContextTransitionStyle = true;
            viewController2.definesPresentationContext = true;
            viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          
            viewController2.delegate = self
            self.present(viewController2, animated: true, completion: {
                viewController2.view.superview?.isUserInteractionEnabled = true
                viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                self.layarView.isHidden = false
                })
        }
    }
    
    @objc func dismissOnTapOutsideModal(){
        self.dismiss(animated: true, completion: nil)
        self.layarView.isHidden = true
    }
    
    @objc func settingMenuButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "PocketSettingController") as! PocketSettingController
        self.layarView.isHidden = true
        
        viewController.providesPresentationContextTransitionStyle = true;
        viewController.definesPresentationContext = true;
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

        viewController.delegate = self
        viewController.delegate2 = self
        self.present(viewController, animated: false, completion: {
            viewController.view.superview?.isUserInteractionEnabled = true
            viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideSettingMenu)))
            self.layarView.isHidden = true
            })
    }
    
    @objc func dismissOnTapOutsideSettingMenu(){
        self.dismiss(animated: false, completion: nil)
        self.layarView.isHidden = true
      }
 
    // T A B L E  V I E W
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            historiPocket = detailPocket["historiPocket"] as? [[String : Any]]
            if historiPocket?.count == 0 {
                return 1
            } else {
                return historiPocket?.count ?? 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "judulCell", for: indexPath)
            let labelTable1 = cell.viewWithTag(1) as! UILabel

            labelTable1.text = "Histori Pocket"
            
            return cell
        } else {
            if self.historiPocket?.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "isiCell", for: indexPath)
                let tipeLabel = cell.viewWithTag(1) as! UILabel
                tipeLabel.isHidden = true
                
                let tanggalLabel = cell.viewWithTag(2) as! UILabel
                tanggalLabel.isHidden = true
                
                let titikView = cell.viewWithTag(3)!
                titikView.isHidden = true
                
                let waktuLabel = cell.viewWithTag(4) as! UILabel
                waktuLabel.isHidden = true
                
                let nominalPembayaranLabel = cell.viewWithTag(5) as! UILabel
                nominalPembayaranLabel.isHidden = true
                
                var transactionImage = cell.viewWithTag(6) as! UIImageView
                var isiNotif = cell.viewWithTag(7) as! UILabel
                var ctaNotif = cell.viewWithTag(8) as! UILabel
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "isiCell", for: indexPath)
                var item: [String : Any]?
                self.historiPocket = detailPocket["historiPocket"] as? [[String : Any]]
                
                item = self.historiPocket?[indexPath.row]
                
                var tipeLabel = cell.viewWithTag(1) as! UILabel
                var tanggalLabel = cell.viewWithTag(2) as! UILabel
                var titikView = cell.viewWithTag(3)!
                var waktuLabel = cell.viewWithTag(4) as! UILabel
                var nominalPembayaranLabel = cell.viewWithTag(5) as! UILabel
                var transactionImage = cell.viewWithTag(6) as! UIImageView
                var isiNotif = cell.viewWithTag(7) as! UILabel
                var ctaNotif = cell.viewWithTag(8) as! UILabel
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "dd MMM yyyy"
                
                var topupPocket = item!["topupPocket"] as? [String : Any]
                var waktuTopup = topupPocket?["waktuTopup"] as? String ?? ""
                var tanggalTopup = topupPocket?["tanggalTopup"] as? String ?? ""
                let tanggalTopupConverted = dateFormatterGet.date(from: tanggalTopup)
                var nominalTopup = topupPocket?["nominalTopup"] as? Int ?? 0
                let formattedNominalTopupAmount = formatter.string(from: nominalTopup as NSNumber) ?? ""
                
                var penarikanPocket = item!["penarikanPocket"] as? [String : Any]
                var waktuPenarikan = penarikanPocket?["waktuPenarikan"] as? String ?? ""
                var tanggalPenarikan = penarikanPocket?["tanggalPenarikan"] as? String ?? ""
                let tanggalPenarikanConverted = dateFormatterGet.date(from: tanggalPenarikan)
                var nominalPenarikan = penarikanPocket?["nominalPenarikan"] as? Int ?? 0
                let formattedNominalPenarikanAmount = formatter.string(from: nominalPenarikan as NSNumber) ?? ""
                
                transactionImage.isHidden = true
                isiNotif.isHidden = true
                ctaNotif.isHidden = true
                
                if penarikanPocket == nil {
                    tipeLabel.text = "Top Up"
                    tanggalLabel.text = "\(dateFormatterPrint.string(from: tanggalTopupConverted!))"
                    waktuLabel.text = "\(waktuTopup)"
                    nominalPembayaranLabel.text = "+Rp\(formattedNominalTopupAmount)"
                    nominalPembayaranLabel.textColor = UIColor(red: 0.29, green: 0.77, blue: 0.37, alpha: 1.00)
                } else if topupPocket == nil {
                    tipeLabel.text = "Penarikan"
                    tanggalLabel.text = "\(dateFormatterPrint.string(from: tanggalPenarikanConverted!))"
                    waktuLabel.text = "\(waktuPenarikan)"
                    nominalPembayaranLabel.text = "-Rp\(formattedNominalPenarikanAmount)"
                    nominalPembayaranLabel.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1)
                }
                
                titikView.layer.cornerRadius = 3.0
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section, indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func pushToPenarikanConfirmationPage(dataNominal: String) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "KonfirmasiPenarikanController") as! KonfirmasiPenarikanController
       
        viewController.nominalPenarikan = dataNominal
        self.historiPocketTableView.reloadData()
        viewController.dataDetailPocket = self.detailPocket
        
        self.navigationController?.pushViewController(viewController, animated: true)
        self.layarView.isHidden = true
    }
    
    func pushToTopupConfirmationPage(dataNominal: String) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "KonfirmasiTopupController") as! KonfirmasiTopupController
        
        viewController.nominalTopup = dataNominal
        viewController.dataDetailPocket = self.detailPocket
        
        self.navigationController?.pushViewController(viewController, animated: true)
        self.layarView.isHidden = true
    }
    
    func pushToEditPocketPage() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EditPocketController") as! EditPocketController
        
        viewController.dataDetailPocket = self.detailPocket
        
        self.navigationController?.pushViewController(viewController, animated: true)
        self.layarView.isHidden = true
    }
    
    func pushToDeletePocketPage() {
        let nominalPocket = detailPocket["nominalPocket"] as? Int ?? 0
        
        if nominalPocket != 0 {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GagalNotificationController") as! GagalNotificationController
        
            viewController.judulNotif = "Gagal Hapus Pocket"
            viewController.isiNotif = "Masih ada saldo yang belum ditarik nih di pocket kamu."
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          
            self.present(viewController, animated: true, completion: {
                viewController.view.superview?.isUserInteractionEnabled = true
                viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                })
        } else {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "HapusPocketController") as! HapusPocketController
            viewController.dataDetailPocket = self.detailPocket
            self.layarView.isHidden = false
            
            viewController.providesPresentationContextTransitionStyle = true;
            viewController.definesPresentationContext = true;
            viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            viewController.delegate = self
            viewController.closeModal = self
            
            self.present(viewController, animated: true, completion: {
                viewController.view.superview?.isUserInteractionEnabled = true
                viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                self.layarView.isHidden = false
            })
        }
    }
    
    func closeModalAndHideView() {
        self.dismiss(animated: true)
        self.layarView.isHidden = true
    }
    
    func pushToDashboardPocketPage() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPocketController") as! DashboardPocketController
        self.navigationController?.pushViewController(viewController, animated: true)
        self.layarView.isHidden = true
        
        let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
        viewController2.notif = "Pocket telah berhasil dihapus"
        viewController2.providesPresentationContextTransitionStyle = true;
        viewController2.definesPresentationContext = true;
        viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

        self.present(viewController2, animated: true, completion: {
            viewController2.view.superview?.isUserInteractionEnabled = true
            viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
            self.layarView.isHidden = true
            })
    }
}
