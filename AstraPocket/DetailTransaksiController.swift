//
//  DetailTransaksiController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 26/12/22.
//

import UIKit
import Alamofire

class DetailTransaksiController: UIViewController {

    @IBOutlet weak var kembaliKeDashboardButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var jumlahBayarLabel: UILabel!
    @IBOutlet weak var itemBarangLabel: UILabel!
    @IBOutlet weak var hargaLabel: UILabel!
    @IBOutlet weak var noTransaksiLabel: UILabel!
    @IBOutlet weak var waktuTransaksiLabel: UILabel!
    @IBOutlet weak var jumlahBayarJudulLabel: UILabel!
    @IBOutlet weak var tipeTransaksiLabel: UILabel!
    @IBOutlet weak var namaMetodeLabel: UILabel!
    
    var dataDetailPocket : [String : Any] = [:]
    var jumlahTopup = ""
    var jumlahPenarikan = ""
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        kembaliKeDashboardButton.addTarget(self, action: #selector(kembaliKeDashboardButtonTapped), for: .touchUpInside)
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
    
        if jumlahTopup != "" {
            jumlahTopup = jumlahTopup.onlyDigits
            let jumlahTopupIntFormat = Int(jumlahTopup)
            let formattedJumlahTopupAmount = self.formatter.string(from: jumlahTopupIntFormat! as NSNumber) ?? ""
            
            hargaLabel.text = "Rp\(formattedJumlahTopupAmount)"
            jumlahBayarLabel.text = "-Rp\(formattedJumlahTopupAmount)"
            jumlahBayarJudulLabel.text = "-Rp\(formattedJumlahTopupAmount)"
            tipeTransaksiLabel.text = "Topup AstraPocket"
            namaMetodeLabel.text = "Detail Metode Topup"
        } else {
            jumlahPenarikan = jumlahPenarikan.onlyDigits
            let jumlahPenarikanIntFormat = Int(jumlahPenarikan)
            let formattedJumlahPenarikanAmount = self.formatter.string(from: jumlahPenarikanIntFormat! as NSNumber) ?? ""
            
            hargaLabel.text = "Rp\(formattedJumlahPenarikanAmount)"
            jumlahBayarLabel.text = "+Rp\(formattedJumlahPenarikanAmount)"
            jumlahBayarJudulLabel.text = "+Rp\(formattedJumlahPenarikanAmount)"
            tipeTransaksiLabel.text = "Penarikan AstraPocket"
            namaMetodeLabel.text = "Detail Alamat Penarikan"
        }
        getHistory()
    }
    
    @objc func kembaliKeDashboardButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getHistory() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let idPocket = dataDetailPocket["id"] as? Int ?? 0
        print("id pocketnya \(idPocket)")
        AF.request("http://localhost:8080/lihat-histori/\(idPocket)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]
                            let code = itemObject?["status"] as? Int
                                
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let namaPocket = self.dataDetailPocket["namaPocket"] as? String ?? ""
                            self.itemBarangLabel.text = "AstraPocket/\(namaPocket)"

                            let noTransaksi = data?[data!.count - 1]["id"] as? Int ?? 0
                            
                            if self.jumlahTopup != "" {
                                let topupPocket = data?[data!.count - 1]["topupPocket"] as? [String: Any]
                                
                                let tanggalTopup = topupPocket?["tanggalTopup"] as? String ?? ""
                                let dateFormatterGetBody = DateFormatter()
                                dateFormatterGetBody.dateFormat = "yyyy-MM-dd"
                                let dateFormatterPrintBody = DateFormatter()
                                dateFormatterPrintBody.dateFormat = "yyMMdd"
                                let tanggalTopupConverted = dateFormatterGetBody.date(from: tanggalTopup)
                                self.noTransaksiLabel.text = "INV/BIL/POC/\(dateFormatterPrintBody.string(from: tanggalTopupConverted!))/\(noTransaksi)"
                        
                                let dateFormatterGetJudul = DateFormatter()
                                dateFormatterGetJudul.dateFormat = "yyyy-MM-dd"
                                let dateFormatterPrintJudul = DateFormatter()
                                dateFormatterPrintJudul.dateFormat = "dd MMM yyyy"
                                let tanggalTopupConvertedJudul = dateFormatterGetJudul.date(from: tanggalTopup)
                                
                                let waktuTopup = topupPocket?["waktuTopup"] as? String ?? ""
                                self.waktuTransaksiLabel.text = "\(dateFormatterPrintJudul.string(from: tanggalTopupConvertedJudul!)), \(waktuTopup) WIB"
                            } else {
                                let penarikanPocket = data?[data!.count - 1]["penarikanPocket"] as? [String: Any]
                                
                                let tanggalPenarikan = penarikanPocket?["tanggalPenarikan"] as? String ?? ""
                                let dateFormatterGetBody = DateFormatter()
                                dateFormatterGetBody.dateFormat = "yyyy-MM-dd"
                                let dateFormatterPrintBody = DateFormatter()
                                dateFormatterPrintBody.dateFormat = "yyMMdd"
                                let tanggalTopupConverted = dateFormatterGetBody.date(from: tanggalPenarikan)
                                self.noTransaksiLabel.text = "INV/BIL/POC/\(dateFormatterPrintBody.string(from: tanggalTopupConverted!))/\(noTransaksi)"
                        
                                let dateFormatterGetJudul = DateFormatter()
                                dateFormatterGetJudul.dateFormat = "yyyy-MM-dd"
                                let dateFormatterPrintJudul = DateFormatter()
                                dateFormatterPrintJudul.dateFormat = "dd MMM yyyy"
                                let tanggalPenarikanConvertedJudul = dateFormatterGetJudul.date(from: tanggalPenarikan)
                                
                                let waktuPenarikan = penarikanPocket?["waktuPenarikan"] as? String ?? ""
                                self.waktuTransaksiLabel.text = "\(dateFormatterPrintJudul.string(from: tanggalPenarikanConvertedJudul!)), \(waktuPenarikan) WIB"
                            }
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
}
