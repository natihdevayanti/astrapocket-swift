//
//  EditEmailProfilController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 03/01/23.
//

import UIKit
import Alamofire

class EditEmailProfilController: UIViewController {

    @IBOutlet weak var perbaruiButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var ubahEmailTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var keteranganEmailLabel: UILabel!
    var dataUser : [String : Any]?
    var email = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        let bottomLineEmail = CALayer()
        bottomLineEmail.frame = CGRect(x: 0.0, y: ubahEmailTextField.frame.height - 1, width: ubahEmailTextField.frame.width, height: 1.0)
        bottomLineEmail.backgroundColor = UIColor.systemGray5.cgColor
        ubahEmailTextField.borderStyle = UITextField.BorderStyle.none
        ubahEmailTextField.layer.addSublayer(bottomLineEmail)
        
        ubahEmailTextField.clearButtonMode = .always
        ubahEmailTextField.clearButtonMode = .whileEditing
        let email = dataUser?["email"] as? String ?? ""
        ubahEmailTextField.text = email
        
        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        perbaruiButton.addTarget(self, action: #selector(perbaruiButtonTapped), for: .touchUpInside)
        
        keteranganEmailLabel.isHidden = true
    }
    
    
    @IBAction func ubahEmailValidation(_ sender: Any) {
        if let email = ubahEmailTextField.text {
            if let errorMessage = invalidEmail(email){
                keteranganEmailLabel.text = errorMessage
                keteranganEmailLabel.isHidden = false
            } else {
                keteranganEmailLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidEmail(_ value: String) -> String? {
        if !emailFormat(value) {
            return "Format email tidak sesuai"
        }
        return nil
    }
        
    func emailFormat(_ value: String) -> Bool {
            let regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkValidasiForm() {
        if ubahEmailTextField.text != "" && keteranganEmailLabel.isHidden  {
            perbaruiButton.isEnabled = true
        } else {
            perbaruiButton.isEnabled = false
        }
    }
    
    @objc func perbaruiButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        email = ubahEmailTextField.text!
        
        let noHP = dataUser?["noHP"] as? String ?? ""
        let nama = dataUser?["nama"] as? String ?? ""
        let tanggalLahir = dataUser?["tanggalLahir"] as? String ?? ""
        let pin = dataUser?["password"] as? String ?? ""
        let photo = dataUser?["photo"] as? String ?? ""
        
        let param = ["nama": nama, "role": "USER", "tanggalLahir": tanggalLahir, "email": email, "noHP": noHP, "password": pin, "photo": photo]

        AF.request("http://localhost:8080/edit-current-user", method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            let code = itemObject?["status"] as? Int
                                
                            
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            viewController2.notif = "Yeay, kamu berhasil ubah email!"
                            viewController2.providesPresentationContextTransitionStyle = true;
                            viewController2.definesPresentationContext = true;
                            viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                            self.present(viewController2, animated: true, completion: {
                                viewController2.view.superview?.isUserInteractionEnabled = true
                                viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                })
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    

}
