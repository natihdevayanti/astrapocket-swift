//
//  EditNamaProfilController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 03/01/23.
//

import UIKit
import Alamofire

class EditNamaProfilController: UIViewController {
    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var perbaruiButton: UIButton!
    
    @IBOutlet weak var buttonView: UIView!
    var dataUser : [String : Any]?
    var nama = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let bottomLineNama = CALayer()
        bottomLineNama.frame = CGRect(x: 0.0, y: namaTextField.frame.height - 1, width: namaTextField.frame.width, height: 1.0)
        bottomLineNama.backgroundColor = UIColor.systemGray5.cgColor
        namaTextField.borderStyle = UITextField.BorderStyle.none
        namaTextField.layer.addSublayer(bottomLineNama)
        
        namaTextField.clearButtonMode = .always
        namaTextField.clearButtonMode = .whileEditing
        let nama = dataUser?["nama"] as? String ?? ""
        namaTextField.text = nama
        
        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        perbaruiButton.addTarget(self, action: #selector(perbaruiButtonTapped), for: .touchUpInside)
    }
    
    
    @IBAction func ubahNamaValidation(_ sender: Any) {
        if let nama = namaTextField.text {
            invalidNama(nama)
        }
        checkValidasiForm()
    }
    
    func invalidNama(_ value: String) -> String? {
        if value == "" {
            return "Nama tidak boleh kosong"
        }
        return nil
    }
    
    func checkValidasiForm() {
        if namaTextField.text != ""  {
            perbaruiButton.isEnabled = true
        } else {
            perbaruiButton.isEnabled = false
        }
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func perbaruiButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        nama = namaTextField.text!
        
        let noHP = dataUser?["noHP"] as? String ?? ""
        let email = dataUser?["email"] as? String ?? ""
        let tanggalLahir = dataUser?["tanggalLahir"] as? String ?? ""
        let pin = dataUser?["password"] as? String ?? ""
        let photo = dataUser?["photo"] as? String ?? ""
        
        let param = ["nama": nama, "role": "USER", "tanggalLahir": tanggalLahir, "email": email, "noHP": noHP, "password": pin, "photo": photo]

        AF.request("http://localhost:8080/edit-current-user", method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            let code = itemObject?["status"] as? Int
                                
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            self.navigationController?.pushViewController(viewController, animated: true)
                                
                            viewController2.notif = "Yeay, kamu berhasil ubah nama!"
                            viewController2.providesPresentationContextTransitionStyle = true;
                            viewController2.definesPresentationContext = true;
                            viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                            self.present(viewController2, animated: true, completion: {
                                viewController2.view.superview?.isUserInteractionEnabled = true
                                viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                })
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    
}
