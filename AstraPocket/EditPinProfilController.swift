//
//  EditPinProfilController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 03/01/23.
//

import UIKit
import Alamofire

class EditPinProfilController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var perbaruiButton: UIButton!
    @IBOutlet weak var ulangPinBaruTextField: UITextField!
    @IBOutlet weak var pinBaruTextField: UITextField!
    @IBOutlet weak var pinLamaTextField: UITextField!
    @IBOutlet weak var eyePasswordUlangPinBaruButton: UIButton!
    @IBOutlet weak var eyePasswordPinBaruButton: UIButton!
    @IBOutlet weak var eyePasswordPinLamaButton: UIButton!
    @IBOutlet weak var keteranganPinLamaLabel: UILabel!
    @IBOutlet weak var keteranganUlangPinBaruLabel: UILabel!
    @IBOutlet weak var keteranganPinBaruLabel: UILabel!
    
    var dataUser : [String : Any]?
//    var pinLama = ""
    var pinBaru = ""
    var ulangPinBaru = ""
    var eyePasswordPinBaruClicked = true
//    var eyePasswordPinLamaClicked = true
    var eyePasswordUlangPinBaruClicked = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let bottomLineUlangPinBaru = CALayer()
        bottomLineUlangPinBaru.frame = CGRect(x: 0.0, y: ulangPinBaruTextField.frame.height - 1, width: ulangPinBaruTextField.frame.width, height: 1.0)
        bottomLineUlangPinBaru.backgroundColor = UIColor.systemGray5.cgColor
        ulangPinBaruTextField.borderStyle = UITextField.BorderStyle.none
        ulangPinBaruTextField.layer.addSublayer(bottomLineUlangPinBaru)

        let bottomLinePinBaru = CALayer()
        bottomLinePinBaru.frame = CGRect(x: 0.0, y: pinBaruTextField.frame.height - 1, width: pinBaruTextField.frame.width, height: 1.0)
        bottomLinePinBaru.backgroundColor = UIColor.systemGray5.cgColor
        pinBaruTextField.borderStyle = UITextField.BorderStyle.none
        pinBaruTextField.layer.addSublayer(bottomLinePinBaru)

//        let bottomLinePinLama = CALayer()
//        bottomLinePinLama.frame = CGRect(x: 0.0, y: pinLamaTextField.frame.height - 1, width: pinLamaTextField.frame.width, height: 1.0)
//        bottomLinePinLama.backgroundColor = UIColor.systemGray5.cgColor
//        pinLamaTextField.borderStyle = UITextField.BorderStyle.none
//        pinLamaTextField.layer.addSublayer(bottomLinePinLama)

        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        perbaruiButton.addTarget(self, action: #selector(perbaruiButtonTapped), for: .touchUpInside)
        perbaruiButton.isEnabled = false
        
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        eyePasswordUlangPinBaruButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        eyePasswordPinBaruButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
//        eyePasswordPinLamaButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        
//        pinLamaTextField.isSecureTextEntry = true
        pinBaruTextField.isSecureTextEntry = true
        ulangPinBaruTextField.isSecureTextEntry = true
        pinBaruTextField.text = ""
        
//        keteranganPinLamaLabel.isHidden = true
        keteranganPinBaruLabel.isHidden = true
        keteranganUlangPinBaruLabel.isHidden = true
        ulangPinBaruTextField.text = ""
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func pinLamaValidation(_ sender: Any) {
//        if let pin = pinLamaTextField.text {
//            if let errorMessage = invalidPin(pin){
//                keteranganPinLamaLabel.text = errorMessage
//                keteranganPinLamaLabel.isHidden = false
//            } else {
//                keteranganPinLamaLabel.isHidden = true
//            }
//        }
//        checkValidasiForm()
//    }

    @IBAction func pinBaruValidation(_ sender: Any) {
        if let pin = pinBaruTextField.text {
            if let errorMessage = invalidPin(pin){
                keteranganPinBaruLabel.text = errorMessage
                keteranganPinBaruLabel.isHidden = false
            } else {
                keteranganPinBaruLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    @IBAction func ulangPinBaruValidation(_ sender: Any) {
        if let pin = ulangPinBaruTextField.text {
            if let errorMessage = invalidUlangiPin(pin){
                keteranganUlangPinBaruLabel.text = errorMessage
                keteranganUlangPinBaruLabel.isHidden = false
            } else {
                keteranganUlangPinBaruLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidPin(_ value: String) -> String? {
        if (value.count != 6){
            return "Panjang PIN harus 6 angka"
        }
        if !containsDigit(value) {
            return "Password harus berupa angka"
        }
        return nil
    }
    
    func invalidUlangiPin(_ value: String) -> String? {
        if (value.count != 6){
            return "Panjang PIN harus 6 angka"
        }
        if !containsDigit(value) {
            return "PIN harus berupa angka"
        }
        if value != pinBaruTextField.text {
            return "PIN tidak sama"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidasiForm() {
        if  keteranganPinBaruLabel.isHidden && keteranganUlangPinBaruLabel.isHidden && pinBaruTextField.text != "" && ulangPinBaruTextField.text != ""  {
            perbaruiButton.isEnabled = true
        } else {
            perbaruiButton.isEnabled = false
        }
    }
    
    @objc func perbaruiButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        pinBaru = pinBaruTextField.text!
        ulangPinBaru = ulangPinBaruTextField.text!
//        pinLama = dataUser?["password"] as? String ?? ""
        
        let noHP = dataUser?["noHP"] as? String ?? ""
        let nama = dataUser?["nama"] as? String ?? ""
        let tanggalLahir = dataUser?["tanggalLahir"] as? String ?? ""
        let email = dataUser?["email"] as? String ?? ""
        let photo = dataUser?["photo"] as? String ?? ""
        
        let param = ["nama": nama, "role": "USER", "tanggalLahir": tanggalLahir, "email": email, "noHP": noHP, "password": ulangPinBaru, "photo": photo]

        AF.request("http://localhost:8080/edit-current-user", method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            } 
                            
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                            self.navigationController?.navigationBar.isHidden = true
                            self.navigationController?.pushViewController(viewController, animated: true)
                            
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            viewController2.notif = "Yeay, kamu berhasil ubah pin!"
                            viewController2.providesPresentationContextTransitionStyle = true;
                            viewController2.definesPresentationContext = true;
                            viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                            self.present(viewController2, animated: true, completion: {
                                viewController2.view.superview?.isUserInteractionEnabled = true
                                viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                })
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func eyePasswordPinLamaHandler(_ sender: Any) {
//        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
//        if eyePasswordPinLamaClicked {
//            pinLamaTextField.isSecureTextEntry = false
//            eyePasswordPinLamaButton.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallConfig), for: .normal)
//        } else {
//            pinLamaTextField.isSecureTextEntry = true
//            eyePasswordPinLamaButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
//        }
//        eyePasswordPinLamaClicked = !eyePasswordPinLamaClicked
//    }
    
    @IBAction func eyePasswordPinBaruHandler(_ sender: Any) {
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        if eyePasswordPinBaruClicked {
            pinBaruTextField.isSecureTextEntry = false
            eyePasswordPinBaruButton.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallConfig), for: .normal)
        } else {
            pinBaruTextField.isSecureTextEntry = true
            eyePasswordPinBaruButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        }
        eyePasswordPinBaruClicked = !eyePasswordPinBaruClicked
    }
    
    
    @IBAction func eyePasswordUlangPinBaruHandler(_ sender: Any) {
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        if eyePasswordUlangPinBaruClicked {
            ulangPinBaruTextField.isSecureTextEntry = false
            eyePasswordUlangPinBaruButton.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallConfig), for: .normal)
        } else {
            ulangPinBaruTextField.isSecureTextEntry = true
            eyePasswordUlangPinBaruButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        }
        eyePasswordUlangPinBaruClicked = !eyePasswordUlangPinBaruClicked
    }
}

