//
//  EditPocketController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class EditPocketController: UIViewController, UITextFieldDelegate,  UIImagePickerControllerDelegate & UINavigationControllerDelegate  {
    
    @IBOutlet weak var nominalTargetTextField: UITextField!
    @IBOutlet weak var namaPocketTextField: UITextField!
    @IBOutlet weak var kategoriPocketTextField: UITextField!
    @IBOutlet weak var simpanPocketView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var simpanPocketButton: UIButton!
    @IBOutlet weak var editGambarButton: UIButton!
    @IBOutlet weak var editGambarView: UIView!
    @IBOutlet weak var gambarPocketImage: UIImageView!
    @IBOutlet weak var sisaNominalTargetLabel: UILabel!
    
    var dataDetailPocket : [String : Any] = [:]
    var dataSaldoPocket : [String : Any] = [:]
    var namaPocket = ""
    var nominalTarget = ""
    var gambarPocket = ""
    var itemGambarPocket = ""
    var linkGambar = ""
    var amt: Int = 0
    let formatter = NumberFormatter()
    var imagePicker = UIImagePickerController()
    var image = UIImage()
    var imageName = ""
    var fileName = ""
    var newImageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        simpanPocketView.layer.shadowColor = UIColor.gray.cgColor
        simpanPocketView.layer.shadowOpacity = 0.3
        simpanPocketView.layer.shadowOffset = .zero
        simpanPocketView.layer.shadowRadius = 5
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        simpanPocketButton.addTarget(self, action: #selector(simpanPocketButtonTapped), for: .touchUpInside)
        
        self.navigationController?.navigationBar.isHidden = true

        editGambarView.layer.cornerRadius = 15
        editGambarView.layer.borderWidth = 2.5
        editGambarView.layer.borderColor = UIColor.white.cgColor
    
        let bottomLineNamaPocket = CALayer()
        bottomLineNamaPocket.frame = CGRect(x: 0.0, y: nominalTargetTextField.frame.height - 1, width: nominalTargetTextField.frame.width, height: 1.0)
        bottomLineNamaPocket.backgroundColor = UIColor.systemGray5.cgColor
        nominalTargetTextField.borderStyle = UITextField.BorderStyle.none
        nominalTargetTextField.layer.addSublayer(bottomLineNamaPocket)
        nominalTargetTextField.delegate = self
        nominalTargetTextField.placeholder = updateAmount()
        nominalTargetTextField.clearButtonMode = .always
        nominalTargetTextField.clearButtonMode = .whileEditing
        
        let bottomLineNominalTarget = CALayer()
        bottomLineNominalTarget.frame = CGRect(x: 0.0, y: namaPocketTextField.frame.height - 1, width: namaPocketTextField.frame.width, height: 1.0)
        bottomLineNominalTarget.backgroundColor = UIColor.systemGray5.cgColor
        namaPocketTextField.borderStyle = UITextField.BorderStyle.none
        namaPocketTextField.layer.addSublayer(bottomLineNominalTarget)
        namaPocketTextField.clearButtonMode = .always
        namaPocketTextField.clearButtonMode = .whileEditing
        
        itemGambarPocket = dataDetailPocket["gambarPocket"] as? String ?? ""
        linkGambar = "http://localhost:8080/files/\(itemGambarPocket)\(":.+")"
        gambarPocketImage.kf.setImage(with: URL(string: linkGambar))
        gambarPocketImage.layer.cornerRadius = gambarPocketImage.layer.frame.width / 2
        
        let nominalTarget = dataDetailPocket["nominalTarget"] as? Int ?? 0
        let formattedNominalTargetAmount = formatter.string(from: nominalTarget as NSNumber) ?? ""
        nominalTargetTextField.text = "\(formattedNominalTargetAmount)"
        
        let namaPocket = dataDetailPocket["namaPocket"] as? String
        namaPocketTextField.text = namaPocket
        
        getPocket()
    }
    
    @IBAction func nominalValidation(_ sender: Any) {
        if let nominal = Int(nominalTargetTextField.text!.onlyDigits) {
            if let errorMessage = invalidNominal(nominal){
                sisaNominalTargetLabel.text = errorMessage
                sisaNominalTargetLabel.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1.0)
                sisaNominalTargetLabel.isHidden = false
            } else {
                let sisaNominalTarget = dataSaldoPocket["sisaNominalTarget"] as? Int ?? 0
                let formattedSisaNominalTargetAmount = self.formatter.string(from: sisaNominalTarget as NSNumber) ?? ""
                self.sisaNominalTargetLabel.text = "Maksimum nominal target adalah \(formattedSisaNominalTargetAmount)"
                sisaNominalTargetLabel.textColor = .black
            }
        }
        checkValidasiForm()
    }
    
    @IBAction func namaPocketValidation(_ sender: Any) {
        let namaPocket = namaPocketTextField.text!
        invalidNamaPocket(namaPocket)
        checkValidasiForm()
    }

    func invalidNamaPocket(_ value: String) -> String? {
        if (value == ""){
            return "Nama tidak boleh kosong"
        }
        return nil
    }

    func invalidNominal(_ value: Int) -> String? {
        let sisaNominalTarget = dataSaldoPocket["sisaNominalTarget"] as? Int ?? 0
        let nominalPocket = dataDetailPocket["nominalPocket"] as? Int ?? 0
        
        if (value > sisaNominalTarget){
            return "Nominal target tidak boleh melebihi batas maksimum nominal target"
        }
        if (value == 0){
            return "Minimum nominal target adalah Rp1"
        }
        if (value < nominalPocket){
            return "Nominal target tidak boleh kurang dari nominal pocket yang sudah diisi"
        }
        return nil
    }
    
    func checkValidasiForm() {
        if sisaNominalTargetLabel.textColor != UIColor(red: 255, green: 0, blue: 0, alpha: 1.0) && nominalTargetTextField.text != "Rp" && namaPocketTextField.text != "" && nominalTargetTextField.text != "" {
            simpanPocketButton.isEnabled = true
        } else {
            simpanPocketButton.isEnabled = false
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let digit = Int(string) {
            amt = amt * 10 + digit
            nominalTargetTextField.text = updateAmount()
        }
        if string == "" {
            amt = amt/10
            nominalTargetTextField.text = amt == 0 ? "" : updateAmount()
        }
        
        nominalValidation(amt)
        
        return false
    }
    
    func updateAmount() -> String? {
        formatter.numberStyle = NumberFormatter.Style.currency
        let amount = amt
        return formatter.string(from: NSNumber(value: amount))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editGambarButtonTapped(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        gambarPocketImage.image = image
        self.image = image
    }
    
    @objc func simpanPocketButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let idPocket = dataDetailPocket["id"] as? Int ?? 0
        
        namaPocket = namaPocketTextField.text!
        nominalTarget = nominalTargetTextField.text!
        nominalTarget = nominalTarget.onlyDigits
        
        self.image = gambarPocketImage.image!
        let newFileName = "pocket_photo_\(Int.random(in: 0 ... 100000000000)).jpeg"
        
        AF.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(self.image.jpegData(compressionQuality: 0.1)!, withName: "file" , fileName: newFileName, mimeType: "image/jpeg")
                print("gambarnya adalah \(newFileName)")
            },
            to: "http://localhost:8080/upload-file", method: .post , headers: headers)
        .response { resp in
            print(resp)
        }
        
        itemGambarPocket = dataDetailPocket["gambarPocket"] as? String ?? ""
        linkGambar = "http://localhost:8080/files/\(itemGambarPocket)\(":.+")"
        gambarPocketImage.kf.setImage(with: URL(string: linkGambar))
    
        let param = ["namaPocket": namaPocket, "nominalTarget": nominalTarget, "gambarPocket": newFileName ]
       
        AF.request("http://localhost:8080/edit-pocket-detail/\(idPocket)", method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        let itemObject = response.value as? [String : Any]
                        let data = itemObject?["data"] as? [[String : Any]]
                        
                        let code = itemObject?["status"] as? Int
                        if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                            self.navigationController?.navigationBar.isHidden = true
                        }
                        
                        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPocketController") as! DashboardPocketController
                        self.navigationController?.pushViewController(viewController, animated: true)
                        
                        let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                        viewController2.notif = "Pocket telah berhasil diedit"
                        viewController2.providesPresentationContextTransitionStyle = true;
                        viewController2.definesPresentationContext = true;
                        viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        
                        self.present(viewController2, animated: true, completion: {
                            viewController2.view.superview?.isUserInteractionEnabled = true
                            viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                        })
                        
                    case .failure(let error):
                        print(error)
                    }
                })
     
    }
        
        
        @objc func dismissOnTapOutsideModal(){
            self.dismiss(animated: true, completion: nil)
        }
        
        func getPocket() {
            let token = UserDefaults.standard.object(forKey: "loginToken") as! String
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            AF.request("http://localhost:8080/lihat-saldo-pocket", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseJSON(completionHandler: { response in
                    switch response.result {
                    case .success:
                        let itemObject = response.value as? [String : Any]
                        let data = itemObject?["data"] as? [String : Any]
                        
                        let sisaNominalTarget = data?["sisaNominalTarget"] as? Int ?? 0
                        let formattedSisaNominalTargetAmount = self.formatter.string(from: sisaNominalTarget as NSNumber) ?? ""
                        self.sisaNominalTargetLabel.text = "Maksimum nominal target adalah \(formattedSisaNominalTargetAmount)"
                  
                        self.dataSaldoPocket = data!

                        let code = itemObject?["status"] as? Int
                        if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                            self.navigationController?.navigationBar.isHidden = true
                        }
                        
                    case .failure(let error):
                        print(error)
                    }
                })
        }
}
