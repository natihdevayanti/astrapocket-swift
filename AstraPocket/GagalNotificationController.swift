//
//  GagalNotificationController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 12/12/22.
//

import UIKit

class GagalNotificationController: UIViewController {

    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var isiNotifLabel: UILabel!
    @IBOutlet weak var judulNotifLabel: UILabel!
    
    var judulNotif = ""
    var isiNotif = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        modalView.layer.cornerRadius = 20.0
        modalView.layer.shadowColor = UIColor.gray.cgColor
        modalView.layer.shadowOpacity = 0.3
        modalView.layer.shadowOffset = .zero
        modalView.layer.shadowRadius = 5
        
        okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
        
        isiNotifLabel.text = isiNotif
        judulNotifLabel.text = judulNotif
    }
    
    @objc func okButtonTapped(){
        self.dismiss(animated: true)
    }
}
