//
//  HapusPocketController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 13/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class HapusPocketController: UIViewController{

    @IBOutlet weak var hapusButton: UIButton!
    @IBOutlet weak var kembaliButton: UIButton!
    @IBOutlet weak var modalView: UIView!
    
    var dataDetailPocket : [String : Any] = [:]
    var delegate: GoToDashboardPocketPageDelegate?
    var closeModal: CloseModalDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        modalView.layer.cornerRadius = 20
        modalView.layer.shadowColor = UIColor.gray.cgColor
        modalView.layer.shadowOpacity = 0.3
        modalView.layer.shadowOffset = .zero
        modalView.layer.shadowRadius = 5
        
        kembaliButton.layer.borderWidth = 1
        kembaliButton.layer.borderColor = UIColor.blue.cgColor
        kembaliButton.layer.cornerRadius = 5.0
        
        kembaliButton.addTarget(self, action: #selector(kembaliButtonTapped), for: .touchUpInside)
        hapusButton.addTarget(self, action: #selector(hapusButtonTapped), for: .touchUpInside)
    }

    @objc func kembaliButtonTapped(){
        closeModal?.closeModalAndHideView()
    }

    @objc func hapusButtonTapped(){
        self.dismiss(animated: false)
       
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        let idPocketDetail = dataDetailPocket["id"] as? Int ?? 0
      
        AF.request("http://localhost:8080/hapus-pocket-detail/\(idPocketDetail)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]
                            
                            self.delegate?.pushToDashboardPocketPage()
                           
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }

                        case .failure(let error):
                            print(error)
                        }
                    })
    }
}
