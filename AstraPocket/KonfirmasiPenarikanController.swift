//
//  KonfirmasiWithdrawController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class KonfirmasiPenarikanController: UIViewController {

    @IBOutlet weak var alamatPenarikanView: UIView!
    @IBOutlet weak var konfirmasiButton: UIButton!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saldoWalletLabel: UILabel!
    @IBOutlet weak var totalPenarikanLabel: UILabel!
    @IBOutlet weak var nominalPenarikanLabel: UILabel!
    @IBOutlet weak var namaPocketLabel: UILabel!
    
    var dataDetailPocket : [String : Any] = [:]
    var nominalPenarikan = ""
    var konfirmasi = false
    let formatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alamatPenarikanView.layer.cornerRadius = 10
        alamatPenarikanView.layer.borderWidth = 1
        alamatPenarikanView.layer.borderColor = UIColor.systemGray5.cgColor

        buttonView.layer.shadowColor = UIColor.gray.cgColor
        buttonView.layer.shadowOpacity = 0.3
        buttonView.layer.shadowOffset = .zero
        buttonView.layer.shadowRadius = 5
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        konfirmasiButton.addTarget(self, action: #selector(konfirmasiButtonTapped), for: .touchUpInside)

        var namaPocket = dataDetailPocket["namaPocket"] as? String
        namaPocketLabel.text = namaPocket
        
        nominalPenarikan = nominalPenarikan.onlyDigits
        let nominalPenarikanIntFormat = Int(nominalPenarikan)
        let formattedNominalPenarikanAmount = self.formatter.string(from: nominalPenarikanIntFormat! as NSNumber) ?? ""
        nominalPenarikanLabel.text = "Rp\(formattedNominalPenarikanAmount)"
        totalPenarikanLabel.text = "Rp\(formattedNominalPenarikanAmount)"
       
        getWallet()
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
   
    @objc func konfirmasiButtonTapped() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        let idPocket = dataDetailPocket["id"] as? Int ?? 0
        konfirmasi = true
        let param = ["nominalPenarikan": nominalPenarikan, "konfirmasi": konfirmasi, "buktiTopup": nil] as [String : Any?]
        
        AF.request("http://localhost:8080/penarikan-pocket/\(idPocket)", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [[String : Any]]
                            
                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BuktiPenarikanController") as! BuktiPenarikanController
                            viewController.jumlahPenarikan = self.nominalPenarikan
                            viewController.dataDetailPocket = self.dataDetailPocket
                            self.navigationController?.pushViewController(viewController, animated: true)
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    func getWallet() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-wallet-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            
                            var saldo = data?["saldo"] as? Int ?? 0
                            let formattedSaldoAmount = self.formatter.string(from: saldo as NSNumber) ?? ""
                            self.saldoWalletLabel.text = "Rp\(formattedSaldoAmount)"

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }

}
