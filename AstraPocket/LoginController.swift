//
//  LoginController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 05/12/22.
//

import UIKit
import Alamofire

class LoginController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var noHPTextField: UITextField!
    @IBOutlet weak var keteranganPasswordLabel: UILabel!
    @IBOutlet weak var keteranganNoHPLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var eyePasswordButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    var eyePasswordClicked = true
    var pass = ""
    var noHP = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.isEnabled = false
        loginButton.backgroundColor = UIColor(red: 0.93, green: 0.91, blue: 0.03, alpha: 0.70)
        loginButton.layer.cornerRadius = 10.0
        loginButton.layer.masksToBounds = true
        
        keteranganPasswordLabel.isHidden = true
        keteranganNoHPLabel.isHidden = true
        
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        registerButton.addTarget(self, action: #selector(registerButtonTapped), for: .touchUpInside)

        noHPTextField.clearButtonMode = .always
        noHPTextField.clearButtonMode = .whileEditing

        isLoggedIn();
        
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        eyePasswordButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)

        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
    @objc func loginButtonTapped() {
        pass = passwordTextField.text!
        noHP = noHPTextField.text!
        
        let param = ["noHP":noHP,"password": pass]

        AF.request("http://localhost:8080/login", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            let token = data?["token"] as? String
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            
                            UserDefaults.standard.setValue(token, forKey: "loginToken")
                            UserDefaults.standard.synchronize()

                            if (token != nil) {
                                self.navigationController?.pushViewController(viewController, animated: true)
                                
                                viewController2.notif = "Yeay, kamu berhasil masuk!"
                                viewController2.providesPresentationContextTransitionStyle = true;
                                viewController2.definesPresentationContext = true;
                                viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                                self.present(viewController2, animated: true, completion: {
                                    viewController2.view.superview?.isUserInteractionEnabled = true
                                    viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                    })
                            }  else {
                                let viewController3 = self.storyboard?.instantiateViewController(withIdentifier: "GagalNotificationController") as! GagalNotificationController
                                
                                viewController3.judulNotif = "Gagal Masuk"
                                viewController3.isiNotif = "Akun kamu belum terdaftar, nih. Yuk, daftar terlebih dahulu!"
                                viewController3.providesPresentationContextTransitionStyle = true;
                                viewController3.definesPresentationContext = true;
                                viewController3.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                                self.present(viewController3, animated: true, completion: {
                                    viewController3.view.superview?.isUserInteractionEnabled = true
                                    viewController3.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                    })
                            }
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    
    func isLoggedIn() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as? String
        if token ?? "" != "" {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "BerandaController") as! BerandaController

            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }

    
    @objc func registerButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterController") as! RegisterController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func passwordValidation(_ sender: Any) {
        if let password = passwordTextField.text {
            if let errorMessage = invalidPassword(password){
                keteranganPasswordLabel.text = errorMessage
                keteranganPasswordLabel.isHidden = false
            } else {
                keteranganPasswordLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidPassword(_ value: String) -> String? {
        if (value.count != 6){
            return "Panjang PIN harus 6 angka"
        }
        if !containsDigit(value) {
            return "Password harus berupa angka"
        }
        return nil
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    @IBAction func eyePasswordHandler(_ sender: Any) {
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        if eyePasswordClicked {
            passwordTextField.isSecureTextEntry = false
            eyePasswordButton.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallConfig), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            eyePasswordButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        }
        eyePasswordClicked = !eyePasswordClicked
    }

    @IBAction func noHPValidation(_ sender: Any) {
        if let noHP = noHPTextField.text {
            if let errorMessage = invalidNoHP(noHP){
                keteranganNoHPLabel.text = errorMessage
                keteranganNoHPLabel.isHidden = false
            } else {
                keteranganNoHPLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidNoHP(_ value: String) -> String? {
        if (value.count < 10){
            return "Panjang nomor HP minimal 10"
        }
        if !containsDigit(value) {
            return "No HP harus berupa angka"
        }
        return nil
    }
    

    func checkValidasiForm() {
        if keteranganPasswordLabel.isHidden && keteranganNoHPLabel.isHidden && noHPTextField.text != "" && passwordTextField.text != "" {
            loginButton.isEnabled = true
        } else {
            loginButton.isEnabled = false
            loginButton.backgroundColor = UIColor(red: 0.93, green: 0.91, blue: 0.03, alpha: 0.70)
            loginButton.layer.cornerRadius = 10.0
            loginButton.layer.masksToBounds = true
        }
    }
    
}
