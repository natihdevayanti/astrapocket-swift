//
//  LogoutConfirmationController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 21/12/22.
//

import UIKit
import Alamofire

class LogoutConfirmationController: UIViewController {

    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var kembaliButton: UIButton!
    
    var delegate: GoToLoginPageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        modalView.layer.cornerRadius = 20
        modalView.layer.shadowColor = UIColor.gray.cgColor
        modalView.layer.shadowOpacity = 0.3
        modalView.layer.shadowOffset = .zero
        modalView.layer.shadowRadius = 5
        
        kembaliButton.layer.borderWidth = 1
        kembaliButton.layer.borderColor = UIColor.blue.cgColor
        kembaliButton.layer.cornerRadius = 5.0

        logoutButton.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
    }
    
    @objc func logoutButtonTapped(){
        self.dismiss(animated: false)
       
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
 
        UserDefaults.standard.removeObject(forKey: "loginToken")
        self.navigationController?.navigationBar.isHidden = true
       
        delegate?.pushToLoginPage()
    }
}
