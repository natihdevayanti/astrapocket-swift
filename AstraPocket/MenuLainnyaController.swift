//
//  MenuLainnyaController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 21/12/22.
//

import UIKit

class MenuLainnyaController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var astraPocketLogoImage: UIImageView!
    @IBOutlet weak var astraPocketLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // create tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MenuLainnyaController.imageTapped(gesture:)))
           // add it to the image view;
        astraPocketLogoImage.addGestureRecognizer(tapGesture)
           // make sure imageView can be interacted with by user
        astraPocketLogoImage.isUserInteractionEnabled = true
  
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
    }
    
    @objc func backButtonTapped() {
        self.navigationController?.popViewController( animated: true)
    }

    @objc func imageTapped(gesture: UIGestureRecognizer) {
        // if the tapped view is a UIImageView then set it to imageview
        if (gesture.view as? UIImageView) != nil {
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPocketController") as! DashboardPocketController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}
