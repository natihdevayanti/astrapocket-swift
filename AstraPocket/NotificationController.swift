//
//  NotificationController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 13/12/22.
//

import UIKit

class NotificationController: UIViewController {

    @IBOutlet weak var notificationModalView: UIView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var notifLabel: UILabel!
    
    var notif = "Pocket telah berhasil dibuat"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationModalView.layer.cornerRadius = 20.0
        notificationModalView.layer.shadowColor = UIColor.gray.cgColor
        notificationModalView.layer.shadowOpacity = 0.3
        notificationModalView.layer.shadowOffset = .zero
        notificationModalView.layer.shadowRadius = 5
        
        okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
        
        notifLabel.text = notif
    }
    
    @objc func okButtonTapped(){
        self.dismiss(animated: true)
    }
}
