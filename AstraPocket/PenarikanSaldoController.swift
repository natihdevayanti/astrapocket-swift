//
//  PenarikanSaldoController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 07/12/22.
//

import UIKit
import Alamofire

class PenarikanSaldoController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var selanjutnyaButton: UIButton!
    @IBOutlet weak var nominalTextField: UITextField!
    @IBOutlet weak var upModalView: UIView!
    @IBOutlet weak var popupModalView: UIView!
    @IBOutlet weak var sisaSaldoPocketLabel: UILabel!
    @IBOutlet weak var saldoWalletLabel: UILabel!
    
    var delegate: GoToPenarikanConfirmationPageDelegate?
    var dataDetailPocket : [String : Any] = [:]
    var dataSaldoPocket : [String : Any] = [:]
    let formatter = NumberFormatter()
    var amt: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        popupModalView.layer.cornerRadius = 20.0
        popupModalView.layer.shadowColor = UIColor.gray.cgColor
        popupModalView.layer.shadowOpacity = 0.3
        popupModalView.layer.shadowOffset = .zero
        popupModalView.layer.shadowRadius = 5
        upModalView.layer.cornerRadius = 5.0
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        selanjutnyaButton.addTarget(self, action: #selector(selanjutnyaButtonTapped), for: .touchUpInside)
        selanjutnyaButton.isEnabled = false
        
        let bottomLineNominalPocket = CALayer()
        bottomLineNominalPocket.frame = CGRect(x: 0.0, y: nominalTextField.frame.height - 1, width: nominalTextField.frame.width, height: 1.0)
        bottomLineNominalPocket.backgroundColor = UIColor.systemGray5.cgColor
        
        nominalTextField.delegate = self
        nominalTextField.placeholder = updateAmount()
        nominalTextField.borderStyle = UITextField.BorderStyle.none
        nominalTextField.layer.addSublayer(bottomLineNominalPocket)
        
        let nominalPenarikan = dataDetailPocket["nominalPenarikan"] as? String ?? ""
        nominalTextField.text = "Rp\(nominalPenarikan)"
        nominalTextField.clearButtonMode = .always
        nominalTextField.clearButtonMode = .whileEditing
        
        getWallet()
        getPocket()
    }
    
    @IBAction func nominalValidation(_ sender: Any) {
        if let nominal = Int(nominalTextField.text!.onlyDigits){
            if let errorMessage = invalidNominal(nominal) {
                sisaSaldoPocketLabel.text = errorMessage
                sisaSaldoPocketLabel.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1.0)
                sisaSaldoPocketLabel.isHidden = false
            }
//            else if invalidNominalPenarikanTextField(nominalTextField.text ?? "") != nil {
//                let nominalPenarikanTextField = nominalTextField.text ?? ""
//            }
            else {
                let saldo = dataSaldoPocket["saldo"] as? Int ?? 0
                let formattedSaldoAmount = self.formatter.string(from: saldo as NSNumber) ?? ""
                
                self.sisaSaldoPocketLabel.text = "Total tabungan AstraPocket Anda saat ini adalah \(formattedSaldoAmount)"
                sisaSaldoPocketLabel.textColor = .black
            }
        }
        checkValidasiForm()
    }
    
//    func invalidNominalPenarikanTextField(_ value: String) -> String? {
//        if (value == ""){
//            return "Nominal penarikan tidak boleh kosong"
//        }
//        return nil
//    }
    
    func checkValidasiForm() {
        if sisaSaldoPocketLabel.textColor != UIColor(red: 255, green: 0, blue: 0, alpha: 1.0) && nominalTextField.text != "" && nominalTextField.text != "Rp" {
            selanjutnyaButton.isEnabled = true
        } else {
            selanjutnyaButton.isEnabled = false
        }
    }
    
    func invalidNominal(_ value: Int) -> String? {
        let nominalPocket = dataDetailPocket["nominalPocket"] as? Int ?? 0
        
        if (value > nominalPocket){
            return "Nominal penarikan tidak boleh melebihi nominal pocket"
        }
        if (value == 0){
            return "Minimum nominal penarikan adalah Rp1"
        }
        return nil
    }
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let digit = Int(string) {
            amt = amt * 10 + digit
            nominalTextField.text = updateAmount()
        }
        if string == "" {
            amt = amt/10
            nominalTextField.text = amt == 0 ? "" : updateAmount()
        }
        nominalValidation(amt)
        return false
    }
    
    func updateAmount() -> String? {
        formatter.numberStyle = NumberFormatter.Style.currency
        let amount = amt
        
        return formatter.string(from: NSNumber(value: amount))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func selanjutnyaButtonTapped() {
        self.dismiss(animated: false)
        
        let dataNominal = nominalTextField.text ?? ""
        delegate?.pushToPenarikanConfirmationPage(dataNominal: dataNominal)
    }
    
    func getWallet() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-wallet-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            
                            let saldo = data?["saldo"] as? Int ?? 0
                            let formattedSaldoAmount = self.formatter.string(from: saldo as NSNumber) ?? ""
                            self.saldoWalletLabel.text = "\(formattedSaldoAmount)"

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }

    func getPocket() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-saldo-pocket", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                           
                            let saldo = data?["saldo"] as? Int ?? 0
                            let formattedSaldoAmount = self.formatter.string(from: saldo as NSNumber) ?? ""
                            self.sisaSaldoPocketLabel.text = "Total tabungan AstraPocket Anda saat ini adalah \(formattedSaldoAmount)"
                            self.dataSaldoPocket = data!

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
                            
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
}


