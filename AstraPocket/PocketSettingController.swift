//
//  PocketSettingController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 12/12/22.
//

import UIKit

class PocketSettingController: UIViewController {

    @IBOutlet weak var hapusPocketButton: UIButton!
    @IBOutlet weak var editPocketButton: UIButton!
    @IBOutlet weak var popupSettingView: UIView!

    var delegate: GoToEditPocketPageDelegate?
    var delegate2: GoToDeletePocketPageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupSettingView.layer.cornerRadius = 10.0
        popupSettingView.layer.shadowColor = UIColor.gray.cgColor
        popupSettingView.layer.shadowOpacity = 0.3
        popupSettingView.layer.shadowOffset = .zero
        popupSettingView.layer.shadowRadius = 5
        
        editPocketButton.addTarget(self, action: #selector(editPocketButtonTapped), for: .touchUpInside)
        hapusPocketButton.addTarget(self, action: #selector(hapusPocketButtonTapped), for: .touchUpInside)
    }
    
    @objc func editPocketButtonTapped() {
        self.dismiss(animated: false)
        delegate?.pushToEditPocketPage()
    }
    
    @objc func hapusPocketButtonTapped() {
        self.dismiss(animated: false)
        delegate2?.pushToDeletePocketPage()
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
}
