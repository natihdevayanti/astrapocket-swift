//
//  ProfilController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 20/12/22.
//

import UIKit
import Alamofire
import Kingfisher

protocol GoToLoginPageDelegate {
    func pushToLoginPage()
}

class ProfilController: UIViewController, GoToLoginPageDelegate {
    
    @IBOutlet weak var layarView: UIView!
    @IBOutlet weak var upgradeMemberLabel: UILabel!
    @IBOutlet weak var keluarView: UIView!
    @IBOutlet weak var ubahPinView: UIView!
    @IBOutlet weak var ubahEmailView: UIView!
    @IBOutlet weak var ubahNamaView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var profilModalView: UIView!
    @IBOutlet weak var emailNotifView: UIView!
    @IBOutlet weak var noHPLabel: UILabel!
    @IBOutlet weak var namaLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    var dataUser : [String : Any]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profilModalView.layer.cornerRadius = 20
        emailNotifView.layer.cornerRadius = 10
        layarView.isHidden = true

        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        
        let tapKeluar = UITapGestureRecognizer(target: self, action: #selector(logoutConfirmation))
        keluarView.addGestureRecognizer(tapKeluar)
        
        let tapUbahNama = UITapGestureRecognizer(target: self, action: #selector(ubahNamaTapped))
        ubahNamaView.addGestureRecognizer(tapUbahNama)
        
        let tapUbahEmail = UITapGestureRecognizer(target: self, action: #selector(ubahEmailTapped))
        ubahEmailView.addGestureRecognizer(tapUbahEmail)
        
        let tapUbahPin = UITapGestureRecognizer(target: self, action: #selector(ubahPinTapped))
        ubahPinView.addGestureRecognizer(tapUbahPin)
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Upgrade", attributes: underlineAttribute)
        upgradeMemberLabel.attributedText = underlineAttributedString
    
        let nama = dataUser?["nama"] as? String ?? ""
        namaLabel.text = nama
        
        let noHP = dataUser?["noHP"] as? String ?? ""
        noHPLabel.text = noHP
        
        let photo = dataUser?["photo"] as? String ?? ""
        let photoLink = "http://localhost:8080/files/\(photo)\(":.+")"
        userImage.kf.setImage(with: URL(string: photoLink))
    }
    
    @objc func logoutConfirmation() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LogoutConfirmationController") as! LogoutConfirmationController
        layarView.isHidden = false
        
        viewController.providesPresentationContextTransitionStyle = true;
        viewController.definesPresentationContext = true;
        viewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

        viewController.delegate = self
        self.present(viewController, animated: true, completion: {
        viewController.view.superview?.isUserInteractionEnabled = true
        viewController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
            self.layarView.isHidden = false
        })
        
        viewController.kembaliButton.addTarget(self, action: #selector(kembaliButtonTapped), for: .touchUpInside)
    }
    
    @objc func ubahNamaTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EditNamaProfilController") as! EditNamaProfilController
        viewController.dataUser = self.dataUser
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func ubahEmailTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EditEmailProfilController") as! EditEmailProfilController
        viewController.dataUser = self.dataUser
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func ubahPinTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "EditPinProfilController") as! EditPinProfilController
        viewController.dataUser = self.dataUser
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func kembaliButtonTapped(){
        self.dismiss(animated: true)
        self.layarView.isHidden = true
    }
 
    @objc func backButtonTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissOnTapOutsideModal(){
        self.dismiss(animated: true, completion: nil)
        self.layarView.isHidden = true
    }
    
    func pushToLoginPage() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        self.navigationController?.pushViewController(viewController, animated: true)
    
        let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
        viewController2.notif = "Sampai jumpa lagi, ya!"
        viewController2.providesPresentationContextTransitionStyle = true;
        viewController2.definesPresentationContext = true;
        viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

        self.present(viewController2, animated: true, completion: {
            viewController2.view.superview?.isUserInteractionEnabled = true
            viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
            })
    }
    
}

