//
//  RegisterController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 05/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class RegisterController: UIViewController {
    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var daftarButton: UIButton!
    @IBOutlet weak var tanggalLahirTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pinTextField: UITextField!
    @IBOutlet weak var noHPTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var keteranganNoHPLabel: UILabel!
    @IBOutlet weak var keteranganEmailLabel: UILabel!
    @IBOutlet weak var keteranganTanggalLahirLabel: UILabel!
    @IBOutlet weak var keteranganPinLabel: UILabel!
    @IBOutlet weak var eyePasswordButton: UIButton!
    
    var pin = ""
    var noHP = ""
    var email = ""
    var tanggalLahir = ""
    var nama = ""
    var eyePasswordClicked = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        daftarButton.addTarget(self, action: #selector(daftarButtonTapped), for: .touchUpInside)
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        
        daftarButton.backgroundColor = UIColor(red: 0.93, green: 0.91, blue: 0.03, alpha: 0.70)
        daftarButton.layer.cornerRadius = 10.0
        daftarButton.layer.masksToBounds = true
        daftarButton.isEnabled = false
     
        keteranganNoHPLabel.isHidden = true
        keteranganEmailLabel.isHidden = true
        keteranganPinLabel.isHidden = true
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(dateChanged(datePicker:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 300)
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = Date()
        tanggalLahirTextField.inputView = datePicker
        
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        eyePasswordButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
        pinTextField.isSecureTextEntry = true

        self.navigationController?.navigationBar.backItem?.title = ""
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func eyePasswordHandler(_ sender: Any) {
            let smallConfig = UIImage.SymbolConfiguration(scale: .small)
            if eyePasswordClicked {
                pinTextField.isSecureTextEntry = false
                eyePasswordButton.setImage(UIImage(systemName: "eye.fill", withConfiguration: smallConfig), for: .normal)
            } else {
                pinTextField.isSecureTextEntry = true
                eyePasswordButton.setImage(UIImage(systemName: "eye.slash.fill", withConfiguration: smallConfig), for: .normal)
            }
            eyePasswordClicked = !eyePasswordClicked
    }

    @objc func daftarButtonTapped() {
        pin = pinTextField.text!
        noHP = noHPTextField.text!
        nama = namaTextField.text!
        email = emailTextField.text!
        tanggalLahir = tanggalLahirTextField.text!
        
        let param = ["nama": nama, "noHP": noHP,"password": pin, "email": email, "tanggalLahir": tanggalLahir, "role": "USER", "photo": "profile-icon.png"]

        AF.request("http://localhost:8080/register", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                        
                            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
                            let viewController2 = self.storyboard?.instantiateViewController(withIdentifier: "NotificationController") as! NotificationController
                            
                            let viewController3 = self.storyboard?.instantiateViewController(withIdentifier: "GagalNotificationController") as! GagalNotificationController
                            
                            let code = itemObject?["code"] as? Int
                           
                            if code == 400 {
                                viewController3.isiNotif = "Akun kamu sudah pernah terdaftar, nih. Silakan ke halaman login, ya!"
                                viewController3.judulNotif = "Gagal Daftar Akun"
                                
                                viewController3.providesPresentationContextTransitionStyle = true;
                                viewController3.definesPresentationContext = true;
                                viewController3.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext

                                self.present(viewController3, animated: true, completion: {
                                    viewController3.view.superview?.isUserInteractionEnabled = true
                                    viewController3.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                    })
                            } else {
                                self.navigationController?.pushViewController(viewController, animated: true)
                                
                                viewController2.notif = "Yeay, kamu berhasil terdaftar!"
                                viewController2.providesPresentationContextTransitionStyle = true;
                                viewController2.definesPresentationContext = true;
                                viewController2.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                
                                self.present(viewController2, animated: true, completion: {
                                    viewController2.view.superview?.isUserInteractionEnabled = true
                                    viewController2.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutsideModal)))
                                })
                            }
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func loginButtonTapped() {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func dismissOnTapOutsideModal(){
         self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        tanggalLahirTextField.text = formatDate(date: datePicker.date)
    }
    
    func formatDate(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date)
    }
    
    
    @IBAction func noHPValidation(_ sender: Any) {
        if let noHP = noHPTextField.text {
            if let errorMessage = invalidNoHP(noHP){
                keteranganNoHPLabel.text = errorMessage
                keteranganNoHPLabel.isHidden = false
            } else {
                keteranganNoHPLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidNoHP(_ value: String) -> String? {
        if (value.count < 10){
            return "Panjang nomor HP minimal 10"
        }
        if !containsDigit(value) {
            return "No HP harus berupa angka"
        }
        return nil
    }
    
    
    @IBAction func emailValidation(_ sender: Any) {
        if let email = emailTextField.text {
            if let errorMessage = invalidEmail(email){
                keteranganEmailLabel.text = errorMessage
                keteranganEmailLabel.isHidden = false
            } else {
                keteranganEmailLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidEmail(_ value: String) -> String? {
        if !emailFormat(value) {
            return "Format email tidak sesuai"
        }
        return nil
    }

    @IBAction func pinValidation(_ sender: Any) {
        if let pin = pinTextField.text {
            if let errorMessage = invalidPin(pin){
                keteranganPinLabel.text = errorMessage
                keteranganPinLabel.isHidden = false
            } else {
                keteranganPinLabel.isHidden = true
            }
        }
        checkValidasiForm()
    }
    
    func invalidPin(_ value: String) -> String? {
        if (value.count != 6){
            return "Panjang PIN harus 6 angka"
        }
        if !containsDigit(value) {
            return "Password harus berupa angka"
        }
        return nil
    }
    
    func checkValidasiForm() {
        if keteranganPinLabel.isHidden && keteranganNoHPLabel.isHidden && namaTextField.text != "" && noHPTextField.text != "" && emailTextField.text != "" && tanggalLahirTextField.text != "" && pinTextField.text != "" {
            daftarButton.isEnabled = true
        } else {
            daftarButton.backgroundColor = UIColor(red: 0.93, green: 0.91, blue: 0.03, alpha: 0.70)
            daftarButton.layer.cornerRadius = 10.0
            daftarButton.layer.masksToBounds = true
            daftarButton.isEnabled = false
        }
    }
    
    func containsDigit(_ value: String) -> Bool {
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func emailFormat(_ value: String) -> Bool {
        let regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
            + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
}
