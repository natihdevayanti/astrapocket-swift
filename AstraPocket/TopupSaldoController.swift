//
//  TopupSaldoController.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 12/12/22.
//

import UIKit
import Alamofire

class TopupSaldoController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nominalTextField: UITextField!
    @IBOutlet weak var upModalView: UIView!
    @IBOutlet weak var modalPopupView: UIView!
    @IBOutlet weak var selanjutnyaButton: UIButton!
    @IBOutlet weak var saldoWalletLabel: UILabel!
    @IBOutlet weak var sisaPencapaianTargetLabel: UILabel!
    @IBOutlet weak var oneHundredPercentButton: UIButton!
    @IBOutlet weak var eightyPercentButton: UIButton!
    @IBOutlet weak var sixtyPercentButton: UIButton!
    @IBOutlet weak var fortyPercentButton: UIButton!
    @IBOutlet weak var twentyPercentButton: UIButton!
    @IBOutlet weak var notifSaldoLabel: UILabel!
    
    var delegate: GoToTopupConfirmationPageDelegate?
    var dataDetailPocket : [String : Any] = [:]
    var dataWallet: [String : Any] = [:]
    let formatter = NumberFormatter()
    var saldo = 0
    var amt: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        modalPopupView.layer.cornerRadius = 20.0
        modalPopupView.layer.shadowColor = UIColor.gray.cgColor
        modalPopupView.layer.shadowOpacity = 0.3
        modalPopupView.layer.shadowOffset = .zero
        modalPopupView.layer.shadowRadius = 5
        upModalView.layer.cornerRadius = 5.0
        
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        
        twentyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.layer.cornerRadius = 10
    
        selanjutnyaButton.addTarget(self, action: #selector(selanjutnyaButtonTapped), for: .touchUpInside)
        selanjutnyaButton.isEnabled = false
        
        twentyPercentButton.addTarget(self, action: #selector(twentyPercentButtonTapped), for: .touchUpInside)
        fortyPercentButton.addTarget(self, action: #selector(fortyPercentButtonTapped), for: .touchUpInside)
        sixtyPercentButton.addTarget(self, action: #selector(sixtyPercentButtonTapped), for: .touchUpInside)
        eightyPercentButton.addTarget(self, action: #selector(eightyPercentButtonTapped), for: .touchUpInside)
        oneHundredPercentButton.addTarget(self, action: #selector(oneHundredPercentButtonTapped), for: .touchUpInside)
        
        let bottomLineNominalPocket = CALayer()
        bottomLineNominalPocket.frame = CGRect(x: 0.0, y: nominalTextField.frame.height - 1, width: nominalTextField.frame.width, height: 1.0)
        bottomLineNominalPocket.backgroundColor = UIColor.systemGray5.cgColor
        
        nominalTextField.delegate = self
        nominalTextField.placeholder = updateAmount()
        nominalTextField.borderStyle = UITextField.BorderStyle.none
        nominalTextField.layer.addSublayer(bottomLineNominalPocket)
    
        let nominalPocket = dataDetailPocket["nominalPocket"] as? Int ?? 0
        let formattedNominalPocketAmount = self.formatter.string(from: nominalPocket as NSNumber) ?? ""
        
        let nominalTarget = dataDetailPocket["nominalTarget"] as? Int ?? 0
        let formattedNominalTargetAmount = self.formatter.string(from: nominalTarget as NSNumber) ?? ""
        
        let sisaPencapaianTarget = nominalTarget - nominalPocket
        let formattedSisaPencapaianTargetAmount = self.formatter.string(from: sisaPencapaianTarget as NSNumber) ?? ""
        sisaPencapaianTargetLabel.text = "Sisa \(formattedSisaPencapaianTargetAmount) untuk mencapai target"
        
        let nominalTopup = dataDetailPocket["nominalTopup"] as? String ?? ""
        nominalTextField.text = "Rp\(nominalTopup)"
        
        nominalTextField.clearButtonMode = .always
        nominalTextField.clearButtonMode = .whileEditing
        
        notifSaldoLabel.isHidden = true
        getWallet()

    }
    
    @IBAction func nominalValidation(_ sender: Any) {
        if let nominal = Int(nominalTextField.text!.onlyDigits) {
            if let errorMessage = invalidSaldo(nominal){
                notifSaldoLabel.text = errorMessage
                notifSaldoLabel.isHidden = false
            } else {
                notifSaldoLabel.isHidden = true
            }
    
            if let errorMessage = invalidNominal(nominal){
                sisaPencapaianTargetLabel.text = errorMessage
                sisaPencapaianTargetLabel.textColor = UIColor(red: 255, green: 0, blue: 0, alpha: 1.0)
                sisaPencapaianTargetLabel.isHidden = false
            } else {
                let nominalTarget = dataDetailPocket["nominalTarget"] as? Int ?? 0
                let nominalPocket = dataDetailPocket["nominalPocket"] as? Int ?? 0
                let sisaPencapaianTarget = nominalTarget - nominalPocket
                let formattedSisaPencapaianTargetAmount = self.formatter.string(from: sisaPencapaianTarget as NSNumber) ?? ""
               
                sisaPencapaianTargetLabel.text = "Sisa \(formattedSisaPencapaianTargetAmount) untuk mencapai target"
                sisaPencapaianTargetLabel.textColor = .black
            }
        }
        checkValidasiForm()
    }
    
    func invalidSaldo(_ value: Int) -> String? {
        if (value > saldo){
            return "Saldo tidak cukup"
        }
        return nil
    }
  
    func invalidNominal(_ value: Int) -> String? {
        let nominalTarget = dataDetailPocket["nominalTarget"] as? Int ?? 0
        let nominalPocket = dataDetailPocket["nominalPocket"] as? Int ?? 0
        let sisaPencapaianTarget = nominalTarget - nominalPocket
        
        if (value > sisaPencapaianTarget){
            return "Nominal topup tidak boleh melebihi nominal target yang ditetapkan"
        }
        if (value > 10000000){
            return "Kamu hanya boleh topup hingga Rp10.000.000 saja nih!"
        }
        if (value == 0){
            return "Minimum nominal topup adalah Rp1"
        }
        return nil
    }
    
    func checkValidasiForm() {
        if notifSaldoLabel.isHidden && sisaPencapaianTargetLabel.textColor != UIColor(red: 255, green: 0, blue: 0, alpha: 1.0) && nominalTextField.text != "" && nominalTextField.text != "Rp" {
            selanjutnyaButton.isEnabled = true
        } else {
            selanjutnyaButton.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let digit = Int(string) {
            amt = amt * 10 + digit
            nominalTextField.text = updateAmount()
        }
        if string == "" {
            amt = amt/10
            nominalTextField.text = amt == 0 ? "" : updateAmount()
        }
        nominalValidation(amt)
        return false
    }
    
    func updateAmount() -> String? {
        formatter.numberStyle = NumberFormatter.Style.currency
        let amount = amt
        
        return formatter.string(from: NSNumber(value: amount))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func selanjutnyaButtonTapped() {
        self.dismiss(animated: false)
        
        let dataNominal = nominalTextField.text ?? ""
        delegate?.pushToTopupConfirmationPage(dataNominal: dataNominal)
    }
    
    func getWallet() {
        let token = UserDefaults.standard.object(forKey: "loginToken") as! String
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
        
        AF.request("http://localhost:8080/lihat-wallet-user", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let itemObject = response.value as? [String : Any]
                            let data = itemObject?["data"] as? [String : Any]
                            
                            self.saldo = data?["saldo"] as? Int ?? 0
                            let formattedSaldoAmount = self.formatter.string(from: self.saldo as NSNumber) ?? ""
                            self.saldoWalletLabel.text = "\(formattedSaldoAmount)"

                            self.dataWallet = data!

                            let code = itemObject?["status"] as? Int
                            if code ?? 0 == 401 {
                            UserDefaults.standard.removeObject(forKey: "loginToken")
                                self.navigationController?.navigationBar.isHidden = true
                            }
               
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    @objc func twentyPercentButtonTapped() {
        twentyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1)
        twentyPercentButton.setTitle("20%", for: .normal)
        twentyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        twentyPercentButton.setTitleColor(.white, for: .normal)
        twentyPercentButton.layer.cornerRadius = 10
        twentyPercentButton.clipsToBounds = true

        fortyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        fortyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.clipsToBounds = true
        fortyPercentButton.setTitle("40%", for: .normal)
        fortyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        fortyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sixtyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sixtyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.clipsToBounds = true
        sixtyPercentButton.setTitle("60%", for: .normal)
        sixtyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sixtyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        eightyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        eightyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.clipsToBounds = true
        eightyPercentButton.setTitle("80%", for: .normal)
        eightyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        eightyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        oneHundredPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        oneHundredPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.clipsToBounds = true
        oneHundredPercentButton.setTitle("100%", for: .normal)
        oneHundredPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        oneHundredPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
       
        let saldo = dataWallet["saldo"] as? Int ?? 0
        var nominalTopup = dataDetailPocket["nominalTopup"] as? Int ?? 0
        
        nominalTopup = 20*saldo/100
        print("nominal topup 20%: \(nominalTopup) dengan saldo \(saldo)")
        let formattedNominalTopupAmount = self.formatter.string(from: nominalTopup as NSNumber) ?? ""
        nominalTextField.text = "\(formattedNominalTopupAmount)"
        nominalValidation(nominalTextField.text?.onlyDigits ?? 0)
    }
    
    @objc func fortyPercentButtonTapped() {
        fortyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        fortyPercentButton.setTitle("40%", for: .normal)
        fortyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        fortyPercentButton.setTitleColor(.white, for: .normal)
        fortyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.clipsToBounds = true
        
        twentyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        twentyPercentButton.layer.cornerRadius = 10
        twentyPercentButton.clipsToBounds = true
        twentyPercentButton.setTitle("20%", for: .normal)
        twentyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        twentyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sixtyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sixtyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.clipsToBounds = true
        sixtyPercentButton.setTitle("60%", for: .normal)
        sixtyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sixtyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        eightyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        eightyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.clipsToBounds = true
        eightyPercentButton.setTitle("80%", for: .normal)
        eightyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        eightyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        oneHundredPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        oneHundredPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.clipsToBounds = true
        oneHundredPercentButton.setTitle("100%", for: .normal)
        oneHundredPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        oneHundredPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        let saldo = dataWallet["saldo"] as? Int ?? 0
        var nominalTopup = dataDetailPocket["nominalTopup"] as? Int ?? 0
        
        nominalTopup = 40*saldo/100
        print("nominal topup 40%: \(nominalTopup) dengan saldo \(saldo)")
        let formattedNominalTopupAmount = self.formatter.string(from: nominalTopup as NSNumber) ?? ""
        nominalTextField.text = "\(formattedNominalTopupAmount)"
        nominalValidation(nominalTextField.text?.onlyDigits ?? 0)
    }
    
    @objc func sixtyPercentButtonTapped() {
        sixtyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        sixtyPercentButton.setTitle("60%", for: .normal)
        sixtyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sixtyPercentButton.setTitleColor(.white, for: .normal)
        sixtyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.clipsToBounds = true
        
        twentyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        twentyPercentButton.layer.cornerRadius = 10
        twentyPercentButton.clipsToBounds = true
        twentyPercentButton.setTitle("20%", for: .normal)
        twentyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        twentyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        fortyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        fortyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.clipsToBounds = true
        fortyPercentButton.setTitle("40%", for: .normal)
        fortyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        fortyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        eightyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        eightyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.clipsToBounds = true
        eightyPercentButton.setTitle("80%", for: .normal)
        eightyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        eightyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        oneHundredPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        oneHundredPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.clipsToBounds = true
        oneHundredPercentButton.setTitle("100%", for: .normal)
        oneHundredPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        oneHundredPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        let saldo = dataWallet["saldo"] as? Int ?? 0
        var nominalTopup = dataDetailPocket["nominalTopup"] as? Int ?? 0
        
        nominalTopup = 60*saldo/100
        print("nominal topup 60%: \(nominalTopup) dengan saldo \(saldo)")
        let formattedNominalTopupAmount = self.formatter.string(from: nominalTopup as NSNumber) ?? ""
        nominalTextField.text = "\(formattedNominalTopupAmount)"
        nominalValidation(nominalTextField.text?.onlyDigits ?? 0)
    }
    
    @objc func eightyPercentButtonTapped() {
        eightyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        eightyPercentButton.setTitle("80%", for: .normal)
        eightyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        eightyPercentButton.setTitleColor(.white, for: .normal)
        eightyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.clipsToBounds = true
        
        twentyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        twentyPercentButton.layer.cornerRadius = 10
        twentyPercentButton.clipsToBounds = true
        twentyPercentButton.setTitle("20%", for: .normal)
        twentyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        twentyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        fortyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        fortyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.clipsToBounds = true
        fortyPercentButton.setTitle("40%", for: .normal)
        fortyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        fortyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sixtyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sixtyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.clipsToBounds = true
        sixtyPercentButton.setTitle("60%", for: .normal)
        sixtyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sixtyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        oneHundredPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        oneHundredPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.clipsToBounds = true
        oneHundredPercentButton.setTitle("100%", for: .normal)
        oneHundredPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        oneHundredPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        let saldo = dataWallet["saldo"] as? Int ?? 0
        var nominalTopup = dataDetailPocket["nominalTopup"] as? Int ?? 0
        
        nominalTopup = 80*saldo/100
        print("nominal topup 80%: \(nominalTopup) dengan saldo \(saldo)")
        let formattedNominalTopupAmount = self.formatter.string(from: nominalTopup as NSNumber) ?? ""
        nominalTextField.text = "\(formattedNominalTopupAmount)"
        nominalValidation(nominalTextField.text?.onlyDigits ?? 0)
    }
    
    @objc func oneHundredPercentButtonTapped() {
        oneHundredPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0)
        oneHundredPercentButton.setTitle("100%", for: .normal)
        oneHundredPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        oneHundredPercentButton.setTitleColor(.white, for: .normal)
        oneHundredPercentButton.layer.cornerRadius = 10
        oneHundredPercentButton.clipsToBounds = true
        
        twentyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        twentyPercentButton.layer.cornerRadius = 10
        twentyPercentButton.clipsToBounds = true
        twentyPercentButton.setTitle("20%", for: .normal)
        twentyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        twentyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        fortyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        fortyPercentButton.layer.cornerRadius = 10
        fortyPercentButton.clipsToBounds = true
        fortyPercentButton.setTitle("40%", for: .normal)
        fortyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        fortyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        sixtyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        sixtyPercentButton.layer.cornerRadius = 10
        sixtyPercentButton.clipsToBounds = true
        sixtyPercentButton.setTitle("60%", for: .normal)
        sixtyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        sixtyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        eightyPercentButton.backgroundColor = UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 0.15)
        eightyPercentButton.layer.cornerRadius = 10
        eightyPercentButton.clipsToBounds = true
        eightyPercentButton.setTitle("80%", for: .normal)
        eightyPercentButton.titleLabel!.font = .systemFont(ofSize: 13, weight: .semibold)
        eightyPercentButton.setTitleColor(UIColor(red: 0.0, green: 0.27, blue: 0.90, alpha: 1.0), for: .normal)
        
        let saldo = dataWallet["saldo"] as? Int ?? 0
        var nominalTopup = dataDetailPocket["nominalTopup"] as? Int ?? 0
        
        nominalTopup = saldo
        print("nominal topup 100%: \(nominalTopup) dengan saldo \(saldo)")
        let formattedNominalTopupAmount = self.formatter.string(from: nominalTopup as NSNumber) ?? ""
        nominalTextField.text = "\(formattedNominalTopupAmount)"
        nominalValidation(nominalTextField.text?.onlyDigits ?? 0)
    }
}
