//
//  ViewController2.swift
//  AstraPay6
//
//  Created by Natih Devayanti on 22/11/22.
//

import UIKit

class ViewController2: UIViewController {


    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var view1: UIView!

    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        image1.kf.setImage(with: URL(string: "https://astrapay.com/static-assets/images/upload/blog/main/2022/10/fraudawareness_onlineshop_blog903x903.png"))
        button1.clipsToBounds = true
        button1.layer.cornerRadius = 20
      
        button2.clipsToBounds = true
        button2.layer.cornerRadius = 20
        
        buttonClose.addTarget(self, action: #selector(buttonCloseTapped), for: .touchUpInside)
        view1.layer.borderWidth = 1
        view1.layer.borderColor = UIColor.gray.cgColor
        view1.layer.cornerRadius = 10
    }
    
    @objc func buttonCloseTapped(){
        self.dismiss(animated: true)
    }
    
}
