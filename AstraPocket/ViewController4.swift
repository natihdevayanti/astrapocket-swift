//
//  ViewController4.swift
//  AstraPocket
//
//  Created by Natih Devayanti on 05/12/22.
//

import UIKit

class ViewController4: UIViewController {

    @IBOutlet weak var ulangiPinTextField: UITextField!
    @IBOutlet weak var lanjutkanButton: UIButton!
    @IBOutlet weak var pinTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        lanjutkanButton.addTarget(self, action: #selector(lanjutkanButtonTapped), for: .touchUpInside)
        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
    @objc func lanjutkanButtonTapped() {
        let viewController5 = self.storyboard?.instantiateViewController(withIdentifier: "ViewController5") as! ViewController5
        
        self.navigationController?.pushViewController(viewController5, animated: true)

    }
}
